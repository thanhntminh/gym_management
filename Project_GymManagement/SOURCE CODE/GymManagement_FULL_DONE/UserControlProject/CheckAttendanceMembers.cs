﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using UserControlProject.DTO;

namespace UserControlProject
{

    public partial class CheckAttendanceMembers : UserControl
    {
        MemberDAO memberDAO = new MemberDAO();
        DataTable dtMember = new DataTable();

        public CheckAttendanceMembers()
        {
            InitializeComponent();
        }

        private void btnSearchMemberInfo_Click(object sender, EventArgs e)
        {
            string memberName = txtMemberName.Text;
            dtMember = memberDAO.searchMember(memberName);
            //Khai bao cot id la khoa chinh
            //dtMember.PrimaryKey = new DataColumn[] { dtMember.Columns[0] };
            dgvListMembers.DataSource = dtMember;

            txtID.DataBindings.Clear();
            txtName.DataBindings.Clear();
            txtPhone.DataBindings.Clear();
            txtEmail.DataBindings.Clear();
            dtpBirthday.DataBindings.Clear();
            txtAddress.DataBindings.Clear();

            txtID.DataBindings.Add("Text", dtMember, "id");
            txtName.DataBindings.Add("Text", dtMember, "name");
            txtPhone.DataBindings.Add("Text", dtMember, "phone");
            txtEmail.DataBindings.Add("Text", dtMember, "email");
            dtpBirthday.DataBindings.Add("Text", dtMember, "birthday");
            txtAddress.DataBindings.Add("Text", dtMember, "address");

            
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            string memberID = txtID.Text;
            DateTime now = DateTime.Now;
            string takeBy = Session.CurrentUser.Username;
            bool isGuest = false;
            AttendanceDAO attendanceDAO = new AttendanceDAO();
            Attendance att = new Attendance()
            {
                memberId = memberID,
                date = now,
                takeBy = takeBy,
                isGuest = isGuest
            };
            string result = attendanceDAO.takeAttendance(att);
            MessageBox.Show(result);
        }
    }
}
