﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using BussinessObject.DTO;
using System.Windows.Forms.DataVisualization.Charting;

namespace UserControlProject
{
    public partial class MemberHistory : UserControl
    {
        MemberDAO memberDAO = new MemberDAO();
        DataTable dtMember = new DataTable();

        public MemberHistory()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string memberName = txtMemberName.Text;
            dtMember = memberDAO.searchMember(memberName);
            //Khai bao cot id la khoa chinh
            //dtMember.PrimaryKey = new DataColumn[] { dtMember.Columns[0] };
            dgvHistory.DataSource = dtMember;
        }

        private void dgvHistory_SelectionChanged(object sender, EventArgs e)
        {
            
        }

        private void loadChart(string memberID)
        {
                History history = new History();
                history = memberDAO.GetHistory(memberID);
                TimeSpan numberOfDay = new TimeSpan();
                numberOfDay = history.endDay.Subtract(history.extendedDay);
                int totalDay = numberOfDay.Days;
                DateTime today = DateTime.Now;
                TimeSpan numberOfUsedDay = new TimeSpan();
                numberOfUsedDay = today.Subtract(history.extendedDay);
                int usedDay = numberOfUsedDay.Days;
                TimeSpan numberOfRemainDay = new TimeSpan();
                numberOfRemainDay = history.endDay.Subtract(today);
                int remainDay = numberOfRemainDay.Days;
                pieChart.Series["Day"].Points.Clear();
                pieChart.Series["Day"].Points.AddXY("Day Used", usedDay);
                pieChart.Series["Day"].Points.AddXY("Day Remaining", remainDay);
            
        }

        private void pieChart_Click(object sender, EventArgs e)
        {

        }

        private void dgvHistory_MouseClick(object sender, MouseEventArgs e)
        {
            if (dgvHistory.DataSource != null)
            {
                int row = 0;
                row = dgvHistory.CurrentCell.RowIndex;
                string memberID = dgvHistory.Rows[row].Cells[0].Value.ToString().Trim();
                loadChart(memberID);
            } else
            {
                MessageBox.Show("No item added!!!");
            }
        }
    }
}
