﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using UserControlProject.DTO;
using System.Text.RegularExpressions;
using BarcodeLib;
using System.IO;

namespace UserControlProject
{
    public partial class AddNewMember : UserControl
    {

        public AddNewMember()
        {
            InitializeComponent();
        }

        private void btnAddNewMember_Click(object sender, EventArgs e)
        {
            bool check = true;
            string sex = "";
            MemberDAO dao = new MemberDAO();
            Member member = new Member();
            DataTable dt = new DataTable();

            if (string.IsNullOrEmpty(txtMemberID.Text))
            {
                lbID.ForeColor = Color.Red;
                lbID.Text = "ID can't be blank!";
                check = false;
            }
            else
            {
                dt = dao.CheckPK(txtMemberID.Text);
                if (dt.Rows.Count > 0)
                {
                    lbID.ForeColor = Color.Red;
                    lbID.Text = "Duplicated ID!";
                    check = false;
                }
                else
                {
                    Regex RegexID = new Regex("[^a-zA-Z0-9]");
                    if (RegexID.IsMatch(txtMemberID.Text))
                    {
                        lbID.ForeColor = Color.Red;
                        lbID.Text = "ID can't contains special character (/*-+@&$#% )!";
                        check = false;
                    }
                    else
                    {
                        if (txtMemberID.Text.Length < 6 || txtMemberID.Text.Length > 9)
                        {
                            lbID.ForeColor = Color.Red;
                            lbID.Text = "ID length must between 6 - 9 character!";
                            check = false;
                        }
                        else
                        {
                            lbID.Text = "";
                        }

                    }
                }

            }


            if (string.IsNullOrEmpty(txtName.Text))
            {
                lbName.ForeColor = Color.Red;
                lbName.Text = "Name can't be blank!";
                check = false;
            }
            else
            {
                if (txtName.Text.Length > 50)
                {
                    lbName.ForeColor = Color.Red;
                    lbName.Text = "Length of name can't be bigger than 50!";
                    check = false;
                }
                else
                {
                    lbName.Text = "";
                }
            }

            if (string.IsNullOrEmpty(txtPhone.Text))
            {
                lbPhone.ForeColor = Color.Red;
                lbPhone.Text = "Phone can't be blank!";
                check = false;
            }
            else
            {
                Regex RegixPhone = new Regex("[^0-9]");
                if (RegixPhone.IsMatch(txtPhone.Text))
                {
                    lbPhone.ForeColor = Color.Red;
                    lbPhone.Text = "Phone number is wrong format!";
                    check = false;

                }
                else
                {
                    if (txtPhone.Text.Length > 15 || txtPhone.Text.Length < 10)
                    {
                        lbPhone.ForeColor = Color.Red;
                        lbPhone.Text = "Length of phone must contain 10-15 digit!";
                        check = false;
                    }
                    else
                    {
                        lbPhone.Text = "";
                    }
                }
            }


            if (string.IsNullOrEmpty(txtEmail.Text))
            {
                lbEmail.ForeColor = Color.Red;
                lbEmail.Text = "Email can't be blank!";
                check = false;
            }
            else
            {
                if (txtEmail.Text.Length > 50)
                {
                    lbEmail.ForeColor = Color.Red;
                    lbEmail.Text = "Length of Email can't be bigger than 50!";
                    check = false;
                }
                else
                {
                    Regex RegexEmail = new Regex("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
                    if (!RegexEmail.IsMatch(txtEmail.Text))
                    {
                        lbEmail.ForeColor = Color.Red;
                        lbEmail.Text = "Email is wrong format";
                        check = false;
                    }
                    else
                    {
                        lbEmail.Text = "";
                    }
                }
            }


            if (string.IsNullOrEmpty(txtAddress.Text))
            {
                lbAddress.ForeColor = Color.Red;
                lbAddress.Text = "Address can't be blank!";
                check = false;
            }
            else
            {
                if (txtAddress.Text.Length > 50)
                {
                    lbAddress.ForeColor = Color.Red;
                    lbAddress.Text = "Length of Address can't be bigger than 100!";
                    check = false;
                }
                else
                {
                    lbAddress.Text = "";
                }
            }


            if (check)
            {
                member.id = txtMemberID.Text;
                member.name = txtName.Text;
                if (rdMale.Checked)
                {
                    sex = "MALE";
                }
                else
                {
                    sex = "FEMALE";
                }
                member.sex = sex;
                member.birthday = DateTime.Parse(dtpBirthday.Text);
                member.phone = txtPhone.Text;
                member.email = txtEmail.Text;
                member.address = txtAddress.Text;
                member.status = Member.STATUS_ACTIVE;
                AddHistoryNewUser frmExtend = new AddHistoryNewUser(member);
                frmExtend.Show();
            }
        }



        private void btnCleartpANM_Click(object sender, EventArgs e)
        {
            txtMemberID.Text = string.Empty;
            txtName.Text = string.Empty;
            rdMale.Checked = true;
            dtpBirthday.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtAddress.Text = string.Empty;
        }
    }
}
