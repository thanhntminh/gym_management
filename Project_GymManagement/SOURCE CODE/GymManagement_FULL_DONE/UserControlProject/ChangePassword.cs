﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;

namespace UserControlProject
{
    public partial class ChangePassword : UserControl
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void btnChangePass_Click(object sender, EventArgs e)
        {
            bool check = true, result = true;
            AccountDAO dao = new AccountDAO();
            string curUsername = Session.CurrentUser.Username;
            string curPass = Session.CurrentUser.Password;

            if (string.IsNullOrEmpty(txtOldPass.Text))
            {
                lbOldPass.ForeColor = Color.Red;
                lbOldPass.Text = "Old Password can't be blank!";
                check = false;
            }
            else
            {
                if (!txtOldPass.Text.Equals(curPass))
                {
                    lbOldPass.ForeColor = Color.Red;
                    lbOldPass.Text = "Old Password is incorrect!";
                    check = false;
                }
                else
                {
                    lbOldPass.Text = "";
                }
            }

            if (string.IsNullOrEmpty(txtNewPass.Text))
            {
                lbNewPass.ForeColor = Color.Red;
                lbNewPass.Text = "New Password can't be blank!";
                check = false;
            }
            else
            {
                if (txtNewPass.Text.Length > 50)
                {
                    lbNewPass.ForeColor = Color.Red;
                    lbNewPass.Text = "Length of New Password can't be bigger than 50!";
                    check = false;
                }
                else
                {
                    lbNewPass.Text = "";
                }
            }

            if (string.IsNullOrEmpty(txtConfirmPass.Text))
            {
                lbConfirmPass.ForeColor = Color.Red;
                lbConfirmPass.Text = "Confirm Password can't be blank!";
                check = false;
            }
            else
            {
                if (!txtConfirmPass.Text.Equals(txtNewPass.Text))
                {
                    lbConfirmPass.ForeColor = Color.Red;
                    lbConfirmPass.Text = "Confirm Password must be equal to New Password!";
                    check = false;
                }
                else
                {
                    lbConfirmPass.Text = "";
                }
            }

            if (check)
            {
                result = dao.ChangePassword(curUsername, txtNewPass.Text);
                string s = (result == true ? "Success" : "Fail");
                MessageBox.Show("Change Pass " + s, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtOldPass.Text = string.Empty;
            txtNewPass.Text = string.Empty;
            txtConfirmPass.Text = string.Empty;
        }
    }
}
