﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZXing;
using TouchlessLib;
using BussinessObject;
using UserControlProject.DTO;
using System.Diagnostics;

namespace UserControlProject
{
    public partial class CheckAttendanceByCode : UserControl
    {
        private IBarcodeReader _barcodeReader;
        private TouchlessMgr _touch;
        private const int _previewWidth = 1500;
        private const int _previewHeight = 1000;

        public CheckAttendanceByCode()
        {
            InitializeComponent();
            // Initialize Dynamsoft Barcode Reader
            _barcodeReader = new BarcodeReader();
            // Initialize Touchless
            _touch = new TouchlessMgr();
        }

        private void takeAttendance()
        {
            string memberID = txtCode.Text;
            DateTime now = DateTime.Now;
            string takeBy = Session.CurrentUser.Username;
            bool isGuest = false;
            AttendanceDAO attendanceDAO = new AttendanceDAO();
            Attendance att = new Attendance()
            {
                memberId = memberID,
                date = now,
                takeBy = takeBy,
                isGuest = isGuest
            };
            string result = attendanceDAO.takeAttendance(att);
            MessageBox.Show(result);
        }

        private void ReadBarcode(Bitmap bitmap)
        {
            // Read barcodes with Dynamsoft Barcode Reader
            Stopwatch sw = Stopwatch.StartNew();
            sw.Start();
            //BarcodeResult[] results = _barcodeReader.DecodeBitmap(bitmap);
            var results = _barcodeReader.Decode(bitmap);
            sw.Stop();
            Console.WriteLine(sw.Elapsed.TotalMilliseconds + "ms");

            // Clear previous results
            txtCode.Clear();

            if (results == null)
            {
                txtCode.Text = "No barcode detected!";
                return;
            }
            else
            {
                txtCode.Text = results.Text;
                takeAttendance();
                StopCamera();
            }

            // Display barcode results

        }

        private void OnImageCaptured(object sender, CameraEventArgs args)
        {
            // Get the bitmap
            Bitmap bitmap = args.Image;

            // Read barcode and show results in UI thread
            this.Invoke((MethodInvoker)delegate
            {
                ptBox.Image = bitmap;
                ReadBarcode(bitmap);
            });
        }

        private void StopCamera()
        {
            btnStart.Text = "Start Webcam";
            if (_touch.CurrentCamera != null)
            {
                _touch.CurrentCamera.OnImageCaptured -= new EventHandler<CameraEventArgs>(OnImageCaptured);
            }
        }

        private void StartCamera()
        {
            if (_touch.Cameras.Count == 0)
            {
                MessageBox.Show("No USB webcam connected");
                btnStart.Text = "Start Webcam";
                return;
            }

            // Start to acquire images
            _touch.CurrentCamera = _touch.Cameras[0];
            _touch.CurrentCamera.CaptureWidth = _previewWidth; // Set width
            _touch.CurrentCamera.CaptureWidth = _previewHeight; // Set height
            _touch.CurrentCamera.OnImageCaptured += new EventHandler<CameraEventArgs>(OnImageCaptured); // Set preview callback function
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            StartCamera();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            StopCamera();
        }
    }
}
