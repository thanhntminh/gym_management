﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using UserControlProject.DTO;

namespace UserControlProject
{
    public partial class CheckAttendanceGuest : UserControl
    {
        public CheckAttendanceGuest()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            int numberTraining = 0;
            bool isNumberTraining = int.TryParse(txtNumberTraining.Text, out numberTraining);
            float payment = 0;
            bool isPayment = float.TryParse(txtPayment.Text, out payment);
            if (!isNumberTraining || !isPayment)
            {
                MessageBox.Show("Please input a digit!!!");
            }
            float totalPayment = payment * numberTraining;
            string takeBy = Session.CurrentUser.Username;
            string memberId = "GUEST";
            DateTime date = DateTime.Now;
            Attendance att = new Attendance()
            {
                memberId = memberId,
                date = date,
                takeBy = takeBy,
                isGuest = true,
                payment = totalPayment
            };
            AttendanceDAO dao = new AttendanceDAO();
            string result = dao.takeAttendanceGuest(att);
            MessageBox.Show(result);
        }
    }
}
