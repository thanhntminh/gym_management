﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using UserControlProject.DTO;
using System.Text.RegularExpressions;

namespace UserControlProject
{
    public partial class MemberInformation : UserControl
    {
        public MemberInformation()
        {
            InitializeComponent();
        }

        bool checkValidation = false;

        private void btnUpdateMemberInfo_Click(object sender, EventArgs e)
        {
            checkValidation = Validation();
            if (checkValidation)
            {
                string sex = "";
                if (rdMale.Checked)
                {
                    sex = "MALE";
                }
                else
                {
                    sex = "FEMALE";
                }
                MemberDAO dao = new MemberDAO();
                Member member = new Member();
                member.name = txtName.Text;
                member.sex = sex;
                member.birthday = DateTime.Parse(dtPickerInfo.Text);
                member.phone = txtPhone.Text;
                member.email = txtEmail.Text;
                member.address = txtAddress.Text;
                member.id = txtIdInfo.Text;
                bool result = dao.UpdateMemberInformation(member);
                string s = (result == true ? "Success" : "Fail");
                MessageBox.Show("Update " + s, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {

            }
        }

        private void btnSearchMemberInfo_Click(object sender, EventArgs e)
        {
            string name = txtNameToSearch.Text;
            MemberDAO dao = new MemberDAO();
            DataTable dt = dao.SearchMember(name);
            dgvListMembers.DataSource = dt;

            txtIdInfo.DataBindings.Clear();
            txtName.DataBindings.Clear();
            rdMale.DataBindings.Clear();
            rdFemale.DataBindings.Clear();
            dtPickerInfo.DataBindings.Clear();
            txtPhone.DataBindings.Clear();
            txtEmail.DataBindings.Clear();
            txtAddress.DataBindings.Clear();

            txtIdInfo.DataBindings.Add("Text", dt, "id");
            txtName.DataBindings.Add("Text", dt, "name");
            rdMale.CheckedChanged += (s, args) => rdFemale.Checked = !rdMale.Checked;
            dtPickerInfo.DataBindings.Add("Value", dt, "birthday");
            txtPhone.DataBindings.Add("Text", dt, "phone");
            txtEmail.DataBindings.Add("Text", dt, "email");
            txtAddress.DataBindings.Add("Text", dt, "address");
        }

        private void btnDisable_Click(object sender, EventArgs e)
        {
            checkValidation = Validation();
            if (checkValidation)
            {
                MemberDAO dao = new MemberDAO();
                bool result = dao.DisableMember(txtIdInfo.Text);
                string s = (result == true ? "Success" : "Fail");
                MessageBox.Show("Disbale " + s, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            checkValidation = Validation();
            if (checkValidation)
            {
                MemberDAO dao = new MemberDAO();
                bool result = dao.ActiveMember(txtIdInfo.Text);
                string s = (result == true ? "Success" : "Fail");
                MessageBox.Show("Active " + s, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {

            }
        }

        private bool Validation()
        {
            bool check = true;
            MemberDAO dao = new MemberDAO();
            Member member = new Member();
            DataTable dt = new DataTable();

            if (String.IsNullOrEmpty(txtName.Text))
            {
                lbName.ForeColor = Color.Red;
                lbName.Text = "Name can't be blank!";
                check = false;
            }
            else
            {
                if (txtName.Text.Length > 50)
                {
                    lbName.ForeColor = Color.Red;
                    lbName.Text = "Length of name can't be bigger than 50!";
                    check = false;
                }
                else
                {
                    lbName.Text = "";
                }
            }

            if (String.IsNullOrEmpty(txtPhone.Text))
            {
                lbPhone.ForeColor = Color.Red;
                lbPhone.Text = "Phone can't be blank!";
                check = false;
            }
            else
            {
                Regex RegixPhone = new Regex("[^0-9]");
                if (RegixPhone.IsMatch(txtPhone.Text))
                {
                    lbPhone.ForeColor = Color.Red;
                    lbPhone.Text = "Phone number is wrong format!";
                    check = false;

                }
                else
                {
                    if (txtPhone.Text.Length > 15 || txtPhone.Text.Length < 10)
                    {
                        lbPhone.ForeColor = Color.Red;
                        lbPhone.Text = "Length of phone must contain 10-15 digit!";
                        check = false;
                    }
                    else
                    {
                        lbPhone.Text = "";
                    }
                }
            }


            if (String.IsNullOrEmpty(txtEmail.Text))
            {
                lbEmail.ForeColor = Color.Red;
                lbEmail.Text = "Email can't be blank!";
                check = false;
            }
            else
            {
                if (txtEmail.Text.Length > 50)
                {
                    lbEmail.ForeColor = Color.Red;
                    lbEmail.Text = "Length of Email can't be bigger than 50!";
                    check = false;
                }
                else
                {
                    Regex RegexEmail = new Regex("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
                    if (!RegexEmail.IsMatch(txtEmail.Text))
                    {
                        lbEmail.ForeColor = Color.Red;
                        lbEmail.Text = "Email is wrong format";
                        check = false;
                    }
                    else
                    {
                        lbEmail.Text = "";
                    }
                }
            }


            if (String.IsNullOrEmpty(txtAddress.Text))
            {
                lbAddress.ForeColor = Color.Red;
                lbAddress.Text = "Address can't be blank!";
                check = false;
            }
            else
            {
                if (txtAddress.Text.Length > 50)
                {
                    lbAddress.ForeColor = Color.Red;
                    lbAddress.Text = "Length of Address can't be bigger than 100!";
                    check = false;
                }
                else
                {
                    lbAddress.Text = "";
                }
            }
            return check;
        }

        private void dgvListMembers_MouseClick(object sender, MouseEventArgs e)
        {
            int index = 0;
            try
            {
                index = dgvListMembers.CurrentCell.RowIndex;
            }
            catch (NullReferenceException ne)
            {
                throw new Exception(ne.Message);
            }

            string gender = dgvListMembers.Rows[index].Cells[2].Value.ToString().Trim();
            if ("MALE".Equals(gender))
            {
                rdMale.Checked = true;
            }
            else
            {
                rdFemale.Checked = true;
            }
        }

        private void dgvListMembers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvListMembers_SelectionChanged(object sender, EventArgs e)
        {
            
        }
    }
}

