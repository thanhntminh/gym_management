﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using COMExcel = Microsoft.Office.Interop.Excel;

namespace UserControlProject
{
    public partial class NumberOfMembers : UserControl
    {

        MemberDAO dao = new MemberDAO();
        DataTable dtMember = new DataTable();

        public NumberOfMembers()
        {
            InitializeComponent();
            loadAllMember();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void loadAllMember()
        {
            try
            {
                dtMember = dao.getAllMember();
                //Them cot stt
                dtMember.Columns.Add("STT", typeof(int)).SetOrdinal(0);
                //set pk
                dtMember.PrimaryKey = new DataColumn[] { dtMember.Columns[1] };

                dgvTotal.DataSource = dtMember;

                //tao stt cho dgv
                for (int i = 0; i < dtMember.Rows.Count; i++)
                {
                    dgvTotal.Rows[i].Cells[0].Value = i + 1;
                }
                dgvTotal.Refresh();
                lbTotal.Text = dtMember.Rows.Count.ToString();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                //Khoi dong excel
                COMExcel.Application exApp = new COMExcel.Application();

                //Them file temp xls
                COMExcel.Workbook exBook = exApp.Workbooks.Add(COMExcel.XlWBATemplate.xlWBATWorksheet);

                //Lay sheet 1
                COMExcel.Worksheet exSheet = (COMExcel.Worksheet)exBook.Worksheets[1];

                //Su dung file excel
                exSheet.Activate();
                exSheet.Name = "Danh sach members";

                //tao range (moi rang la 1 cell trong excel)
                COMExcel.Range range = null;

                //Tao header
                for (int i = 0; i < dgvTotal.ColumnCount; i++)
                {
                    range = (COMExcel.Range)exSheet.Cells[1, i + 1];
                    range.Value2 = dgvTotal.Columns[i].HeaderText;
                    range.Columns.AutoFit();
                }

                //Tao noi dung excel
                for (int i = 0; i < dgvTotal.RowCount; i++)
                {
                    for (int j = 0; j < dgvTotal.ColumnCount; j++)
                    {
                        range = (COMExcel.Range)exSheet.Cells[i + 2, j + 1];
                        range.Value2 = dgvTotal.Rows[i].Cells[j].Value;
                        range.Columns.AutoFit();
                    }
                }
                exApp.Visible = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            loadAllMember();
        }
    }
}
