﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using UserControlProject.DTO;

namespace UserControlProject
{
    public partial class TrainingSession : UserControl
    {
        public TrainingSession()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            DateTime dateFrom = DateTime.Parse(dtpFrom.Text);
            DateTime dateNearTo = DateTime.Parse(dtpTo.Text);
            DateTime dateTo = new DateTime(dateNearTo.Year, dateNearTo.Month, dateNearTo.Day, 23, 59, 59);
            if (dateFrom > dateTo)
            {
                MessageBox.Show("Day FROM can not be after day TO!!!");
                return;
            }
            AttendanceDAO dao = new AttendanceDAO();
            DataTable dt = new DataTable();
            dt = dao.searchDateFromTo(dateFrom, dateTo);
            if (dt.Rows.Count > 0)
            {
                try
                {
                    //Them cot stt
                    dt.Columns.Add("STT", typeof(int)).SetOrdinal(0);
                    //tao stt cho dgv
                    dgvMember.DataSource = dt;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dgvMember.Rows[i].Cells[0].Value = i + 1;
                    }
                    //Them dong Total
                    dt.Rows.Add(new object[] { null, null, null, null, null, "Total: " + dgvMember.Rows.Count });

                    dgvMember.Refresh();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("No record found. Please choose another period of time!");
            }
        }

        private void dgvMember_MouseClick(object sender, MouseEventArgs e)
        {
            if (dgvMember.DataSource == null)
            {
                MessageBox.Show("No item added!!!");
                return;
            }
            DateTime dateFrom = DateTime.Parse(dtpFrom.Text);
            DateTime dateNearTo = DateTime.Parse(dtpTo.Text);
            DateTime dateTo = new DateTime(dateNearTo.Year, dateNearTo.Month, dateNearTo.Day, 23, 59, 59);
            DataTable dtNumber = new DataTable();
            DataTable dtTop5 = new DataTable();
            AttendanceDAO dao = new AttendanceDAO();
            dtNumber = dao.GetNumberOfTraining(dateFrom, dateTo);
            dtTop5 = dao.getTopFiveMember(dateFrom, dateTo);
            int row = 0;
            row = dgvMember.CurrentCell.RowIndex;
            //MessageBox.Show("row: " + row + "     " + dgvMember.RowCount);
            if (row == dgvMember.RowCount - 1)
            {
                //MessageBox.Show("aaaaaaaaaaaaaaaaaaaaaaaaaaa");
                DataTable dtAll = new DataTable();
                dtAll = dao.getAllAtendance();
                int Total = 0;
                Total = dtAll.Rows.Count;
                int numberOfGuest = 0;
                for (int i = 0; i < Total; i++)
                {
                    if ("GUEST".Equals(dtAll.Rows[i][0]))
                    {
                        numberOfGuest++;
                    }
                }
                chartMember.Visible = false;
                chartTotal.Visible = true;
                int numberOfMember = Total - numberOfGuest;
                chartTotal.Series["Total"].Points.Clear();
                chartTotal.Series["Total"].Points.AddXY("Member: " + numberOfMember, numberOfMember);
                chartTotal.Series["Total"].Points.AddXY("Guest: " + numberOfGuest, numberOfGuest);
            }
            else
            {

                string memberID = dgvMember.Rows[row].Cells[1].Value.ToString().Trim();
                //MessageBox.Show(memberID + "    " + dtTop5.Rows.Count);
                bool checkDuplicate = false;
                for (int i = 0; i < dtTop5.Rows.Count; i++)
                {
                    if (dtTop5.Rows[i][0].ToString().Equals(memberID))
                    {
                        checkDuplicate = true;
                    }
                }
                chartMember.Visible = true;
                chartTotal.Visible = false;
                //MessageBox.Show(checkDuplicate.ToString());
                if (checkDuplicate)
                {
                    chartMember.Series["TopFive"].Points.Clear();
                    for (int i = 0; i < dtTop5.Rows.Count; i++)
                    {
                        chartMember.Series["TopFive"].Points.AddXY(dtTop5.Rows[i][0], dtTop5.Rows[i][1]);
                    }

                }
                else
                {
                    chartMember.Series["TopFive"].Points.Clear();
                    DataTable dtOneMember = new DataTable();
                    dtOneMember = dao.getAttendanceOneMember(memberID);
                    int numberOfTraining = int.Parse(dtOneMember.Rows[0][0].ToString());
                    chartMember.Series["TopFive"].Points.AddXY(memberID, numberOfTraining);
                    for (int i = 0; i < dtTop5.Rows.Count; i++)
                    {
                        chartMember.Series["TopFive"].Points.AddXY(dtTop5.Rows[i][0], dtTop5.Rows[i][1]);
                    }
                }
            }

        }
    }
}
