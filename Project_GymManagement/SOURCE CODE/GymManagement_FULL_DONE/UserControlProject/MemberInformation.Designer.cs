﻿namespace UserControlProject
{
    partial class MemberInformation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbAddress = new System.Windows.Forms.Label();
            this.lbEmail = new System.Windows.Forms.Label();
            this.lbPhone = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.btnActive = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.rdFemale = new System.Windows.Forms.RadioButton();
            this.rdMale = new System.Windows.Forms.RadioButton();
            this.btnDisable = new System.Windows.Forms.Button();
            this.btnUpdateMemberInfo = new System.Windows.Forms.Button();
            this.dtPickerInfo = new System.Windows.Forms.DateTimePicker();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtIdInfo = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.dgvListMembers = new System.Windows.Forms.DataGridView();
            this.btnSearchMemberInfo = new System.Windows.Forms.Button();
            this.txtNameToSearch = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListMembers)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.lbAddress);
            this.panel1.Controls.Add(this.lbEmail);
            this.panel1.Controls.Add(this.lbPhone);
            this.panel1.Controls.Add(this.lbName);
            this.panel1.Controls.Add(this.btnActive);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.rdFemale);
            this.panel1.Controls.Add(this.rdMale);
            this.panel1.Controls.Add(this.btnDisable);
            this.panel1.Controls.Add(this.btnUpdateMemberInfo);
            this.panel1.Controls.Add(this.dtPickerInfo);
            this.panel1.Controls.Add(this.txtAddress);
            this.panel1.Controls.Add(this.txtEmail);
            this.panel1.Controls.Add(this.txtPhone);
            this.panel1.Controls.Add(this.txtName);
            this.panel1.Controls.Add(this.txtIdInfo);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.dgvListMembers);
            this.panel1.Controls.Add(this.btnSearchMemberInfo);
            this.panel1.Controls.Add(this.txtNameToSearch);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(955, 700);
            this.panel1.TabIndex = 0;
            // 
            // lbAddress
            // 
            this.lbAddress.AutoSize = true;
            this.lbAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lbAddress.Location = new System.Drawing.Point(510, 425);
            this.lbAddress.Name = "lbAddress";
            this.lbAddress.Size = new System.Drawing.Size(0, 20);
            this.lbAddress.TabIndex = 78;
            // 
            // lbEmail
            // 
            this.lbEmail.AutoSize = true;
            this.lbEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lbEmail.Location = new System.Drawing.Point(510, 365);
            this.lbEmail.Name = "lbEmail";
            this.lbEmail.Size = new System.Drawing.Size(0, 20);
            this.lbEmail.TabIndex = 77;
            // 
            // lbPhone
            // 
            this.lbPhone.AutoSize = true;
            this.lbPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lbPhone.Location = new System.Drawing.Point(510, 309);
            this.lbPhone.Name = "lbPhone";
            this.lbPhone.Size = new System.Drawing.Size(0, 20);
            this.lbPhone.TabIndex = 76;
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lbName.Location = new System.Drawing.Point(510, 173);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(0, 20);
            this.lbName.TabIndex = 75;
            // 
            // btnActive
            // 
            this.btnActive.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnActive.Image = global::UserControlProject.Properties.Resources.icons8_Ok_40;
            this.btnActive.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnActive.Location = new System.Drawing.Point(818, 461);
            this.btnActive.Name = "btnActive";
            this.btnActive.Size = new System.Drawing.Size(94, 46);
            this.btnActive.TabIndex = 74;
            this.btnActive.Text = "Active";
            this.btnActive.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnActive.UseVisualStyleBackColor = true;
            this.btnActive.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(510, 191);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 20);
            this.label1.TabIndex = 73;
            this.label1.Text = "Sex";
            // 
            // rdFemale
            // 
            this.rdFemale.AutoSize = true;
            this.rdFemale.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.rdFemale.Location = new System.Drawing.Point(659, 189);
            this.rdFemale.Name = "rdFemale";
            this.rdFemale.Size = new System.Drawing.Size(69, 20);
            this.rdFemale.TabIndex = 72;
            this.rdFemale.Text = "Female";
            this.rdFemale.UseVisualStyleBackColor = true;
            // 
            // rdMale
            // 
            this.rdMale.AutoSize = true;
            this.rdMale.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.rdMale.Location = new System.Drawing.Point(572, 189);
            this.rdMale.Name = "rdMale";
            this.rdMale.Size = new System.Drawing.Size(54, 20);
            this.rdMale.TabIndex = 71;
            this.rdMale.Text = "Male";
            this.rdMale.UseVisualStyleBackColor = true;
            // 
            // btnDisable
            // 
            this.btnDisable.AutoSize = true;
            this.btnDisable.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnDisable.Image = global::UserControlProject.Properties.Resources.icons8_Unavailable_401;
            this.btnDisable.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDisable.Location = new System.Drawing.Point(685, 461);
            this.btnDisable.Name = "btnDisable";
            this.btnDisable.Size = new System.Drawing.Size(108, 46);
            this.btnDisable.TabIndex = 70;
            this.btnDisable.Text = "Disable";
            this.btnDisable.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDisable.UseVisualStyleBackColor = true;
            this.btnDisable.Click += new System.EventHandler(this.btnDisable_Click);
            // 
            // btnUpdateMemberInfo
            // 
            this.btnUpdateMemberInfo.AutoSize = true;
            this.btnUpdateMemberInfo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnUpdateMemberInfo.Image = global::UserControlProject.Properties.Resources.icons8_Sign_Up_503;
            this.btnUpdateMemberInfo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdateMemberInfo.Location = new System.Drawing.Point(553, 461);
            this.btnUpdateMemberInfo.Name = "btnUpdateMemberInfo";
            this.btnUpdateMemberInfo.Size = new System.Drawing.Size(109, 46);
            this.btnUpdateMemberInfo.TabIndex = 69;
            this.btnUpdateMemberInfo.Text = "Update";
            this.btnUpdateMemberInfo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpdateMemberInfo.UseVisualStyleBackColor = true;
            this.btnUpdateMemberInfo.Click += new System.EventHandler(this.btnUpdateMemberInfo_Click);
            // 
            // dtPickerInfo
            // 
            this.dtPickerInfo.Location = new System.Drawing.Point(572, 232);
            this.dtPickerInfo.Name = "dtPickerInfo";
            this.dtPickerInfo.Size = new System.Drawing.Size(340, 20);
            this.dtPickerInfo.TabIndex = 68;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(572, 390);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(340, 20);
            this.txtAddress.TabIndex = 66;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(572, 329);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(340, 20);
            this.txtEmail.TabIndex = 65;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(572, 277);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(340, 20);
            this.txtPhone.TabIndex = 64;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(572, 143);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(340, 20);
            this.txtName.TabIndex = 63;
            // 
            // txtIdInfo
            // 
            this.txtIdInfo.Location = new System.Drawing.Point(572, 93);
            this.txtIdInfo.Name = "txtIdInfo";
            this.txtIdInfo.ReadOnly = true;
            this.txtIdInfo.Size = new System.Drawing.Size(340, 20);
            this.txtIdInfo.TabIndex = 62;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label9.Location = new System.Drawing.Point(510, 392);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 20);
            this.label9.TabIndex = 60;
            this.label9.Text = "Address";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label10.Location = new System.Drawing.Point(510, 331);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 20);
            this.label10.TabIndex = 59;
            this.label10.Text = "Email";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label11.Location = new System.Drawing.Point(510, 279);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 20);
            this.label11.TabIndex = 58;
            this.label11.Text = "Phone";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label12.Location = new System.Drawing.Point(510, 234);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 20);
            this.label12.TabIndex = 57;
            this.label12.Text = "Birthday";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label13.Location = new System.Drawing.Point(510, 143);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 20);
            this.label13.TabIndex = 56;
            this.label13.Text = "Name";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label14.Location = new System.Drawing.Point(510, 95);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(28, 20);
            this.label14.TabIndex = 55;
            this.label14.Text = "ID";
            // 
            // dgvListMembers
            // 
            this.dgvListMembers.AllowUserToAddRows = false;
            this.dgvListMembers.AllowUserToDeleteRows = false;
            this.dgvListMembers.AllowUserToResizeRows = false;
            this.dgvListMembers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListMembers.Location = new System.Drawing.Point(28, 93);
            this.dgvListMembers.Name = "dgvListMembers";
            this.dgvListMembers.Size = new System.Drawing.Size(451, 358);
            this.dgvListMembers.TabIndex = 54;
            this.dgvListMembers.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListMembers_CellContentClick);
            this.dgvListMembers.SelectionChanged += new System.EventHandler(this.dgvListMembers_SelectionChanged);
            this.dgvListMembers.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvListMembers_MouseClick);
            // 
            // btnSearchMemberInfo
            // 
            this.btnSearchMemberInfo.AutoSize = true;
            this.btnSearchMemberInfo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnSearchMemberInfo.Image = global::UserControlProject.Properties.Resources.icons8_Search_401;
            this.btnSearchMemberInfo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearchMemberInfo.Location = new System.Drawing.Point(513, 19);
            this.btnSearchMemberInfo.Name = "btnSearchMemberInfo";
            this.btnSearchMemberInfo.Size = new System.Drawing.Size(102, 46);
            this.btnSearchMemberInfo.TabIndex = 53;
            this.btnSearchMemberInfo.Text = "Search";
            this.btnSearchMemberInfo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearchMemberInfo.UseVisualStyleBackColor = true;
            this.btnSearchMemberInfo.Click += new System.EventHandler(this.btnSearchMemberInfo_Click);
            // 
            // txtNameToSearch
            // 
            this.txtNameToSearch.Location = new System.Drawing.Point(154, 34);
            this.txtNameToSearch.Name = "txtNameToSearch";
            this.txtNameToSearch.Size = new System.Drawing.Size(325, 20);
            this.txtNameToSearch.TabIndex = 52;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.Location = new System.Drawing.Point(24, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 20);
            this.label2.TabIndex = 51;
            this.label2.Text = "Member\'s Name";
            // 
            // MemberInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "MemberInformation";
            this.Size = new System.Drawing.Size(935, 700);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListMembers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDisable;
        private System.Windows.Forms.Button btnUpdateMemberInfo;
        private System.Windows.Forms.DateTimePicker dtPickerInfo;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtIdInfo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView dgvListMembers;
        private System.Windows.Forms.Button btnSearchMemberInfo;
        private System.Windows.Forms.TextBox txtNameToSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rdFemale;
        private System.Windows.Forms.RadioButton rdMale;
        private System.Windows.Forms.Button btnActive;
        private System.Windows.Forms.Label lbAddress;
        private System.Windows.Forms.Label lbEmail;
        private System.Windows.Forms.Label lbPhone;
        private System.Windows.Forms.Label lbName;
    }
}
