﻿using BarcodeLib;
using BussinessObject;
using BussinessObject.DAO;
using BussinessObject.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserControlProject.DTO;

namespace UserControlProject
{
    public partial class AddHistoryNewUser : Form
    {
        public Member member { get; set; }
        public AddHistoryNewUser(Member member)
        {
            InitializeComponent();
            this.member = member;
            txtID.Text = member.id;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            float discount = 0;
            float payment = 0;
            string id = txtID.Text;
            DateTime today = DateTime.Now;
            DateTime endDay = dtpDate.Value;
            if (!float.TryParse(txtDiscount.Text, out discount) || !float.TryParse(txtPayment.Text, out payment))
            {
                MessageBox.Show("Discount and Payment must be a digit!");
                return;
            }
            HistoryDAO hDao = new HistoryDAO();
            History h = new History()
            {
                memberId = id,
                extendedDay = today,
                endDay = endDay,
                payment = payment,
                discount = discount
            };
            MemberDAO mDao = new MemberDAO();
            bool resultAddUser = mDao.AddNewMember(member);
            if (!resultAddUser)
            {
                MessageBox.Show("Can not add new user right now. Please try again latter!");
                this.Close();
            }
            bool result = hDao.extendDay(h);
            if (result)
            {
                Barcode b = new Barcode(txtID.Text, TYPE.CODE128);
                b.IncludeLabel = true;
                b.Encode(TYPE.CODE128, txtID.Text);
                try
                {
                    string imagesDirectory = Path.Combine(Environment.CurrentDirectory, "IMAGES", txtID.Text + ".png");
                    MessageBox.Show(imagesDirectory);
                    b.SaveImage(imagesDirectory, SaveTypes.PNG);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                MessageBox.Show("Extend day successful!");
                this.Close();
            }
            else
            {
                MessageBox.Show("Extend day failed!");
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtPayment.Text = string.Empty;
            txtDiscount.Text = string.Empty;
        }
    }
}
