﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using COMExcel = Microsoft.Office.Interop.Excel;

namespace UserControlProject
{
    public partial class ViewEquipment : UserControl
    {
        public ViewEquipment()
        {
            InitializeComponent();
            LoadListEquipments();
        }

        EquipmentDAO dao = new EquipmentDAO();
        DataTable dt = new DataTable();

        private void LoadListEquipments()
        {
            try
            {
                dt = dao.getAllEquipment();
                //Them cot stt
                dt.Columns.Add("STT", typeof(int)).SetOrdinal(0);
                //set pk
                dt.PrimaryKey = new DataColumn[] { dt.Columns[1] };

                dgvListEquipments.DataSource = dt;

                //tao stt cho dgv
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dgvListEquipments.Rows[i].Cells[0].Value = i + 1;
                }
                dgvListEquipments.Refresh();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadListEquipments();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                //Khoi dong excel
                COMExcel.Application exApp = new COMExcel.Application();

                //Them file temp xls
                COMExcel.Workbook exBook = exApp.Workbooks.Add(COMExcel.XlWBATemplate.xlWBATWorksheet);

                //Lay sheet 1
                COMExcel.Worksheet exSheet = (COMExcel.Worksheet)exBook.Worksheets[1];

                //Su dung file excel
                exSheet.Activate();
                exSheet.Name = "List Equipments";

                //tao range (moi range la 1 cell trong excel)
                COMExcel.Range range = null;

                //Tao header
                for (int i = 0; i < dgvListEquipments.ColumnCount - 3; i++)
                {
                    range = (COMExcel.Range)exSheet.Cells[1, i + 1];
                    range.Value2 = dgvListEquipments.Columns[i].HeaderText;
                    range.Columns.AutoFit();
                }

                //Tao noi dung excel
                for (int i = 0; i < dgvListEquipments.RowCount; i++)
                {
                    for (int j = 0; j < dgvListEquipments.ColumnCount - 3; j++)
                    {
                        range = (COMExcel.Range)exSheet.Cells[i + 2, j + 1];
                        range.Value2 = dgvListEquipments.Rows[i].Cells[j].Value;
                        range.Columns.AutoFit();
                    }
                }
                exApp.Visible = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void dgvListEquipments_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
