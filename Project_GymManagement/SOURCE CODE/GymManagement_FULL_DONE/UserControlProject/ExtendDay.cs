﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using BussinessObject.DAO;
using BussinessObject.DTO;

namespace UserControlProject
{
    public partial class ExtendDay : UserControl
    {
        public ExtendDay()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string memberName = txtSearchName.Text;
            MemberDAO mDao = new MemberDAO();
            DataTable dtMember = new DataTable();
            dtMember = mDao.searchMember(memberName);
            dgvMember.DataSource = dtMember;
        }

        private void dgvMember_MouseClick(object sender, MouseEventArgs e)
        {
            int row = 0;
            row = dgvMember.CurrentCell.RowIndex;
            string memberID = dgvMember.Rows[row].Cells[0].Value.ToString().Trim();
            txtID.Text = memberID;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtPayment.Text = string.Empty;
            txtDiscount.Text = string.Empty;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            float discount = 0;
            float payment = 0;
            string id = txtID.Text;
            DateTime today = DateTime.Now;
            DateTime endDay = dtpDate.Value;
            if (!float.TryParse(txtDiscount.Text, out discount) || !float.TryParse(txtPayment.Text, out payment))
            {
                MessageBox.Show("Discount and Payment must be a digit!");
                return;
            }
            HistoryDAO hDao = new HistoryDAO();
            History hi = hDao.GetHistory(id);
            if (hi != null || hi.endDay < DateTime.Today)
            {
                MessageBox.Show("The member still have day remain!");
                return;
            }
            History h = new History()
            {
                memberId = id,
                extendedDay = today,
                endDay = endDay,
                payment = payment,
                discount = discount
            };
            bool result = hDao.extendDay(h);
            if (result)
            {
                MessageBox.Show("Extend day successful!");
            } else
            {
                MessageBox.Show("Extend day failed!");
            }
        }
    }
}
