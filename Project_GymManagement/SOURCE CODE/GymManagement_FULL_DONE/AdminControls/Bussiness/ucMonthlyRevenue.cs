﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject.DAO;

namespace AdminControls.Bussiness
{
    public partial class ucMonthlyRevenue : UserControl
    {
        public ucMonthlyRevenue()
        {
            InitializeComponent();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            PaymentDAO dao = new PaymentDAO();
            DataTable dtGuest = new DataTable();
            DataTable dtMember = new DataTable();
            DateTime from = DateTime.Parse(dtFrom.Text);
            DateTime to = DateTime.Parse(dtTo.Text);

            try
            {
                dtGuest = dao.GetPaymentGuest(from, to);
                dgvListPaymentGuests.DataSource = dtGuest;
                dtMember = dao.GetPaymentMember(from, to);
                dgvListPaymentMembers.DataSource = dtMember;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            double totalPaymentGuest = 0, payment;

            for (int i = 0; i < dgvListPaymentGuests.RowCount; i++)
            {
                payment = (double)dgvListPaymentGuests.Rows[i].Cells[2].Value;
                totalPaymentGuest += payment;
            }
            lbTotal.Text = totalPaymentGuest.ToString("0") + " VND";

            double totalPaymentMember = 0, paymentMember;

            for (int i = 0; i < dgvListPaymentMembers.RowCount; i++)
            {
                paymentMember = (double)dgvListPaymentMembers.Rows[i].Cells[2].Value;
                totalPaymentMember += paymentMember;
            }
            lbTotalMember.Text = totalPaymentMember.ToString("0") + " VND";
        }
    }
}
