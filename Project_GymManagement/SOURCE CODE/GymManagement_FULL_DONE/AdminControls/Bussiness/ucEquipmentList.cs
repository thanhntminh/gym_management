﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject.DAO;
using UserControlProject.DTO;
using BussinessObject;

namespace AdminControls.Bussiness
{
    public partial class ucEquipmentList : UserControl
    {
        DataTable dt = null;
        public ucEquipmentList()
        {
            InitializeComponent();
            try
            {
                clearAllWarning();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void getAllStaffToolStripButton_Click(object sender, EventArgs e)
        {
            clearAllWarning();
            try
            {
                EquipmentDAO dao = new EquipmentDAO();
                dt = dao.getAllEquipment();
                dgvEquipment.DataSource = dt;
                dgvEquipment.Columns[3].Visible = false;
                dgvEquipment.Columns[5].Visible = false;

                txtId.DataBindings.Clear();
                txtBrand.DataBindings.Clear();
                txtType.DataBindings.Clear();
                txtPrice.DataBindings.Clear();
                txtBoughtDay.DataBindings.Clear();
                txtStatus.DataBindings.Clear();

                txtBoughtDay.DataBindings.Add("Text", dt, "boughtDay");
                txtId.DataBindings.Add("Text", dt, "id");
                txtBrand.DataBindings.Add("Text",dt,"brand");
                txtType.DataBindings.Add("Text", dt, "typeName");
                txtPrice.DataBindings.Add("Text", dt, "price");
                txtStatus.DataBindings.Add("Text", dt, "status");
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }
        public void clearAllWarning()
        {
            lbWarningPrice.Text = "";
            lbWarningBrand.Text = "";
        }
        public Boolean validate()
        {
            Boolean result = true;
            if(txtBrand.Text.Trim() == "") {
                lbWarningBrand.Text = "This field must be filled!";
                result = false;
            }
            try
            {
                if(txtPrice.Text.Trim() != "")
                {
                    float.Parse(txtPrice.Text.Trim());
                }
            }
            catch (Exception)
            {
                lbWarningPrice.Text = "The data format of this field must be float!";
                result = false;
            }
            return result;
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            clearAllWarning();
            if (validate())
            {
                try
                {
                    EquipmentDAO dao = new EquipmentDAO();
                    Equipment dto = new Equipment();
                    dto.id = int.Parse(txtId.Text);
                    dto.brand = txtBrand.Text;
                    dto.price = float.Parse(txtPrice.Text);
                    Boolean rs = dao.UpdateEquipemt(dto);
                    if (rs)
                    {
                        MessageBox.Show("Update Equipment successfully!");
                        getAllStaffToolStripButton_Click(null, null);
                    }
                    else MessageBox.Show("Update Equipment fail!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btnDisable_Click(object sender, EventArgs e)
        {
            Int32 selectedRowCount = dgvEquipment.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (selectedRowCount > 0)
            {
                try
                {
                    EquipmentDAO dao = new EquipmentDAO();
                    for (int i = 0; i < selectedRowCount; i++)
                    {
                        string tmp = dgvEquipment.SelectedRows[i].Cells[0].Value.ToString();
                        dao.Disable(int.Parse(tmp));
                    }
                    getAllStaffToolStripButton_Click(null, null);
                    MessageBox.Show($"Disable {selectedRowCount} row(s) successfully!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btnActive_Click(object sender, EventArgs e)
        {
            Int32 selectedRowCount = dgvEquipment.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (selectedRowCount > 0)
            {
                try
                {
                    EquipmentDAO dao = new EquipmentDAO();
                    for (int i = 0; i < selectedRowCount; i++)
                    {
                        string tmp = dgvEquipment.SelectedRows[i].Cells[0].Value.ToString();
                        dao.Active(int.Parse(tmp));
                    }
                    getAllStaffToolStripButton_Click(null, null);
                    MessageBox.Show($"Active {selectedRowCount} row(s) successfully!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void txtId_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtBoughtDay_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtType_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtBrand_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtStatus_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
