﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;
using System.Text.RegularExpressions;
using AdminControls.Bussiness;

namespace AdminFunction.Bussiness
{
    public partial class ucAccountList : UserControl
    {
        DataTable dt = null;
        frmResetStaffPassword frmChangePassword = null;
        public ucAccountList()
        {
            InitializeComponent();
            try
            {
                clearAllWarning();
            }
            catch (Exception)
            { }
        }
        private void getAllStaffToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                AccountDAO dao = new AccountDAO();
                dt = dao.getAllStaffAccount();
                dgvAllStaff.DataSource = dt;
                dgvAllStaff.Columns[1].Visible = false;
                dgvAllStaff.Columns[3].Visible = false;
                dgvAllStaff.Columns[4].Visible = false;
                dgvAllStaff.Columns[5].Visible = false;
                dgvAllStaff.Columns[6].Visible = false;
                dgvAllStaff.Columns[7].Visible = false;

                txtUsername.DataBindings.Clear();
                txtName.DataBindings.Clear();
                txtPhone.DataBindings.Clear();
                dtpBirthday.DataBindings.Clear();
                txtEmail.DataBindings.Clear();
                txtAddress.DataBindings.Clear();

                txtUsername.DataBindings.Add("Text", dt, "username");
                txtName.DataBindings.Add("Text", dt, "name");
                txtPhone.DataBindings.Add("Text", dt, "phone");
                dtpBirthday.DataBindings.Add("Value", dt, "birthday");
                txtEmail.DataBindings.Add("Text", dt, "email");
                txtAddress.DataBindings.Add("Text", dt, "address");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void getAllStaffToolStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
        private void toolStripSeparator1_Click(object sender, EventArgs e)
        {

        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim() != "")
                try
                {
                    AccountDAO dao = new AccountDAO();
                    dt = dao.getAllStaffByName(txtSearch.Text);
                    dgvAllStaff.DataSource = dt;
                    dgvAllStaff.Columns[1].Visible = false;
                    dgvAllStaff.Columns[3].Visible = false;
                    dgvAllStaff.Columns[4].Visible = false;
                    dgvAllStaff.Columns[5].Visible = false;
                    dgvAllStaff.Columns[6].Visible = false;
                    dgvAllStaff.Columns[7].Visible = false;

                    txtUsername.DataBindings.Clear();
                    txtName.DataBindings.Clear();
                    txtPhone.DataBindings.Clear();
                    dtpBirthday.DataBindings.Clear();
                    txtEmail.DataBindings.Clear();
                    txtAddress.DataBindings.Clear();

                    txtUsername.DataBindings.Add("Text", dt, "username");
                    txtName.DataBindings.Add("Text", dt, "name");
                    txtPhone.DataBindings.Add("Text", dt, "phone");
                    dtpBirthday.DataBindings.Add("Value", dt, "birthday");
                    txtEmail.DataBindings.Add("Text", dt, "email");
                    txtAddress.DataBindings.Add("Text", dt, "address");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }
        private void btnDisable_Click_2(object sender, EventArgs e)
        {
            if (txtUsername.Text != "")
                try
                {
                    if (txtUsername.Text != null && txtUsername.Text != "")
                    {
                        AccountDAO dao = new AccountDAO();
                        if (dao.setStatus(txtUsername.Text, Account.STATUS_DISABLE))
                            MessageBox.Show("The staff has username '" + txtUsername.Text + "' is disable");
                        else MessageBox.Show("Something went wrong! Please try again!");
                        getAllStaffToolStripButton_Click(null, null);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }
        private void btnResetPassword_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text != "")
                try
                {
                    if (frmChangePassword != null)
                    {
                        frmChangePassword.Close();
                    }
                    frmChangePassword = new frmResetStaffPassword(txtUsername.Text);
                    frmChangePassword.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }

        private void btnActive_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text != "")
                try
                {
                    if (txtUsername.Text != null && txtUsername.Text != "")
                    {
                        AccountDAO dao = new AccountDAO();
                        if (dao.setStatus(txtUsername.Text, Account.STATUS_ACTIVE))
                            MessageBox.Show("The staff has username '" + txtUsername.Text + "' is active");
                        else MessageBox.Show("Something went wrong! Please try again!");
                        getAllStaffToolStripButton_Click(null, null);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            clearAllWarning();
            if (txtUsername.Text != "")
                try
                {
                    if (validateAllFields())
                    {
                        Account account = new Account();
                        account.Username = txtUsername.Text;
                        account.Birthday = dtpBirthday.Value.Date;
                        account.Email = txtEmail.Text;
                        account.Address = txtAddress.Text;
                        account.Phone = txtPhone.Text;
                        account.Name = txtName.Text;
                        AccountDAO dao = new AccountDAO();
                        bool result = dao.updateStaff(account);
                        if (result)
                            MessageBox.Show("Update staff successfully!");
                        else MessageBox.Show("Something went wrong, please try again latter!");
                        getAllStaffToolStripButton_Click(null, null);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
        }
        private void clearAllWarning()
        {
            lbEmailWarning.Text = "";
            lbNameWarning.Text = "";
            lbPhoneWarning.Text = "";
        }
        private bool validateAllFields()
        {
            bool result = true;
            if (txtName.Text.Trim() == "")
            {
                lbNameWarning.Text = "Name can't be null";
                result = false;
            }
            if (txtPhone.Text.Trim() != "" && !Regex.IsMatch(txtPhone.Text.Trim(), "^\\d{10,11}$"))
            {
                lbPhoneWarning.Text = "The phone data format must be 10 - 11 digits!";
                result = false;
            }

            if (txtEmail.Text.Trim() != "" &&
                !Regex.IsMatch(txtEmail.Text.Trim(), @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                lbEmailWarning.Text = "Wrong mail data format";
                result = false;
            }
            return result;
        }
    }
}
