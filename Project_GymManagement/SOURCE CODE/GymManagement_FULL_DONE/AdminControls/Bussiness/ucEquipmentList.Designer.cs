﻿namespace AdminControls.Bussiness
{
    partial class ucEquipmentList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucEquipmentList));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnInfo = new System.Windows.Forms.Panel();
            this.lbWarningBrand = new System.Windows.Forms.Label();
            this.txtBoughtDay = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbWarningPrice = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.lbStatus = new System.Windows.Forms.Label();
            this.lbBoughtDay = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lbPrice = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.lbType = new System.Windows.Forms.Label();
            this.txtBrand = new System.Windows.Forms.TextBox();
            this.lbBrand = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.getAllStaffToolStrip = new System.Windows.Forms.ToolStrip();
            this.btnGetAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDisable = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnActive = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.dgvEquipment = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.pnInfo.SuspendLayout();
            this.panel2.SuspendLayout();
            this.getAllStaffToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEquipment)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pnInfo);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.dgvEquipment);
            this.panel1.Location = new System.Drawing.Point(0, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1479, 673);
            this.panel1.TabIndex = 0;
            // 
            // pnInfo
            // 
            this.pnInfo.AutoSize = true;
            this.pnInfo.Controls.Add(this.lbWarningBrand);
            this.pnInfo.Controls.Add(this.txtBoughtDay);
            this.pnInfo.Controls.Add(this.txtId);
            this.pnInfo.Controls.Add(this.label1);
            this.pnInfo.Controls.Add(this.lbWarningPrice);
            this.pnInfo.Controls.Add(this.btnUpdate);
            this.pnInfo.Controls.Add(this.txtStatus);
            this.pnInfo.Controls.Add(this.lbStatus);
            this.pnInfo.Controls.Add(this.lbBoughtDay);
            this.pnInfo.Controls.Add(this.txtPrice);
            this.pnInfo.Controls.Add(this.lbPrice);
            this.pnInfo.Controls.Add(this.txtType);
            this.pnInfo.Controls.Add(this.lbType);
            this.pnInfo.Controls.Add(this.txtBrand);
            this.pnInfo.Controls.Add(this.lbBrand);
            this.pnInfo.Location = new System.Drawing.Point(717, 70);
            this.pnInfo.Name = "pnInfo";
            this.pnInfo.Size = new System.Drawing.Size(386, 502);
            this.pnInfo.TabIndex = 4;
            // 
            // lbWarningBrand
            // 
            this.lbWarningBrand.AutoSize = true;
            this.lbWarningBrand.ForeColor = System.Drawing.Color.Red;
            this.lbWarningBrand.Location = new System.Drawing.Point(116, 84);
            this.lbWarningBrand.Name = "lbWarningBrand";
            this.lbWarningBrand.Size = new System.Drawing.Size(46, 17);
            this.lbWarningBrand.TabIndex = 17;
            this.lbWarningBrand.Text = "label2";
            // 
            // txtBoughtDay
            // 
            this.txtBoughtDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoughtDay.Location = new System.Drawing.Point(131, 214);
            this.txtBoughtDay.Name = "txtBoughtDay";
            this.txtBoughtDay.ReadOnly = true;
            this.txtBoughtDay.Size = new System.Drawing.Size(224, 28);
            this.txtBoughtDay.TabIndex = 16;
            this.txtBoughtDay.TextChanged += new System.EventHandler(this.txtBoughtDay_TextChanged);
            // 
            // txtId
            // 
            this.txtId.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtId.Location = new System.Drawing.Point(131, 9);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(224, 28);
            this.txtId.TabIndex = 15;
            this.txtId.TextChanged += new System.EventHandler(this.txtId_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(64, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 20);
            this.label1.TabIndex = 14;
            this.label1.Text = "Id:";
            // 
            // lbWarningPrice
            // 
            this.lbWarningPrice.AutoSize = true;
            this.lbWarningPrice.ForeColor = System.Drawing.Color.Red;
            this.lbWarningPrice.Location = new System.Drawing.Point(116, 194);
            this.lbWarningPrice.Name = "lbWarningPrice";
            this.lbWarningPrice.Size = new System.Drawing.Size(104, 17);
            this.lbWarningPrice.TabIndex = 13;
            this.lbWarningPrice.Text = "lbWarningPrice";
            // 
            // btnUpdate
            // 
            this.btnUpdate.AutoSize = true;
            this.btnUpdate.ImageIndex = 0;
            this.btnUpdate.ImageList = this.imageList1;
            this.btnUpdate.Location = new System.Drawing.Point(99, 312);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(97, 31);
            this.btnUpdate.TabIndex = 11;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "add.png");
            // 
            // txtStatus
            // 
            this.txtStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatus.Location = new System.Drawing.Point(131, 262);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(224, 28);
            this.txtStatus.TabIndex = 9;
            this.txtStatus.TextChanged += new System.EventHandler(this.txtStatus_TextChanged);
            // 
            // lbStatus
            // 
            this.lbStatus.AutoSize = true;
            this.lbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatus.Location = new System.Drawing.Point(39, 268);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(69, 20);
            this.lbStatus.TabIndex = 8;
            this.lbStatus.Text = "Status:";
            // 
            // lbBoughtDay
            // 
            this.lbBoughtDay.AutoSize = true;
            this.lbBoughtDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBoughtDay.Location = new System.Drawing.Point(3, 218);
            this.lbBoughtDay.Name = "lbBoughtDay";
            this.lbBoughtDay.Size = new System.Drawing.Size(109, 20);
            this.lbBoughtDay.TabIndex = 6;
            this.lbBoughtDay.Text = "Bought day:";
            // 
            // txtPrice
            // 
            this.txtPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrice.Location = new System.Drawing.Point(131, 158);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(224, 28);
            this.txtPrice.TabIndex = 5;
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            // 
            // lbPrice
            // 
            this.lbPrice.AutoSize = true;
            this.lbPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPrice.Location = new System.Drawing.Point(47, 163);
            this.lbPrice.Name = "lbPrice";
            this.lbPrice.Size = new System.Drawing.Size(59, 20);
            this.lbPrice.TabIndex = 4;
            this.lbPrice.Text = "Price:";
            // 
            // txtType
            // 
            this.txtType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtType.Location = new System.Drawing.Point(131, 107);
            this.txtType.Name = "txtType";
            this.txtType.ReadOnly = true;
            this.txtType.Size = new System.Drawing.Size(224, 28);
            this.txtType.TabIndex = 3;
            this.txtType.TextChanged += new System.EventHandler(this.txtType_TextChanged);
            // 
            // lbType
            // 
            this.lbType.AutoSize = true;
            this.lbType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbType.Location = new System.Drawing.Point(49, 110);
            this.lbType.Name = "lbType";
            this.lbType.Size = new System.Drawing.Size(55, 20);
            this.lbType.TabIndex = 2;
            this.lbType.Text = "Type:";
            // 
            // txtBrand
            // 
            this.txtBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBrand.Location = new System.Drawing.Point(131, 57);
            this.txtBrand.Name = "txtBrand";
            this.txtBrand.Size = new System.Drawing.Size(224, 28);
            this.txtBrand.TabIndex = 1;
            this.txtBrand.TextChanged += new System.EventHandler(this.txtBrand_TextChanged);
            // 
            // lbBrand
            // 
            this.lbBrand.AutoSize = true;
            this.lbBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBrand.Location = new System.Drawing.Point(41, 60);
            this.lbBrand.Name = "lbBrand";
            this.lbBrand.Size = new System.Drawing.Size(65, 20);
            this.lbBrand.TabIndex = 0;
            this.lbBrand.Text = "Brand:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.getAllStaffToolStrip);
            this.panel2.Location = new System.Drawing.Point(3, 15);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(559, 49);
            this.panel2.TabIndex = 3;
            // 
            // getAllStaffToolStrip
            // 
            this.getAllStaffToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.getAllStaffToolStrip.ImageScalingSize = new System.Drawing.Size(30, 30);
            this.getAllStaffToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnGetAll,
            this.toolStripSeparator1,
            this.btnDisable,
            this.toolStripSeparator3,
            this.btnActive,
            this.toolStripSeparator4});
            this.getAllStaffToolStrip.Location = new System.Drawing.Point(0, 0);
            this.getAllStaffToolStrip.Name = "getAllStaffToolStrip";
            this.getAllStaffToolStrip.Size = new System.Drawing.Size(295, 37);
            this.getAllStaffToolStrip.TabIndex = 3;
            this.getAllStaffToolStrip.Text = "getAllStaffToolStrip";
            // 
            // btnGetAll
            // 
            this.btnGetAll.Image = ((System.Drawing.Image)(resources.GetObject("btnGetAll.Image")));
            this.btnGetAll.Name = "btnGetAll";
            this.btnGetAll.Size = new System.Drawing.Size(88, 34);
            this.btnGetAll.Text = "Get All";
            this.btnGetAll.Click += new System.EventHandler(this.getAllStaffToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 37);
            // 
            // btnDisable
            // 
            this.btnDisable.Image = ((System.Drawing.Image)(resources.GetObject("btnDisable.Image")));
            this.btnDisable.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDisable.Name = "btnDisable";
            this.btnDisable.Size = new System.Drawing.Size(93, 34);
            this.btnDisable.Text = "Disable";
            this.btnDisable.Click += new System.EventHandler(this.btnDisable_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 37);
            // 
            // btnActive
            // 
            this.btnActive.Image = ((System.Drawing.Image)(resources.GetObject("btnActive.Image")));
            this.btnActive.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnActive.Name = "btnActive";
            this.btnActive.Size = new System.Drawing.Size(84, 34);
            this.btnActive.Text = "Active";
            this.btnActive.Click += new System.EventHandler(this.btnActive_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 37);
            // 
            // dgvEquipment
            // 
            this.dgvEquipment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvEquipment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEquipment.GridColor = System.Drawing.Color.Gainsboro;
            this.dgvEquipment.Location = new System.Drawing.Point(3, 70);
            this.dgvEquipment.Name = "dgvEquipment";
            this.dgvEquipment.RowTemplate.Height = 24;
            this.dgvEquipment.Size = new System.Drawing.Size(708, 502);
            this.dgvEquipment.TabIndex = 2;
            // 
            // ucEquipmentList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "ucEquipmentList";
            this.Size = new System.Drawing.Size(1495, 693);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnInfo.ResumeLayout(false);
            this.pnInfo.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.getAllStaffToolStrip.ResumeLayout(false);
            this.getAllStaffToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEquipment)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip getAllStaffToolStrip;
        private System.Windows.Forms.ToolStripButton btnGetAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.DataGridView dgvEquipment;
        
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn brandDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel pnInfo;
        private System.Windows.Forms.TextBox txtBrand;
        private System.Windows.Forms.Label lbBrand;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Label lbBoughtDay;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label lbPrice;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.Label lbType;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStripButton btnDisable;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnActive;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.Label lbWarningPrice;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBoughtDay;
        private System.Windows.Forms.Label lbWarningBrand;
    }
}
