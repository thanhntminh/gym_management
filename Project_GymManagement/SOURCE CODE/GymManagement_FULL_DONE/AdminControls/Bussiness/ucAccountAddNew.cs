﻿using System;
using System.Windows.Forms;
using BussinessObject;
using System.Text.RegularExpressions;

namespace AdminControls.Bussiness
{
    public partial class ucAccountAddNew : UserControl
    {
        public ucAccountAddNew()
        {
            InitializeComponent();
            clearAllWarning();
        }

        private void txtName_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtPassword.Text = "";
            txtName.Text = "";
            txtPhone.Text = "";
            txtEmail.Text = "";
            txtUsername.Text = "";
            txtAddress.Text = "";
        }
        private void clearAllWarning()
        {
            lbEmailWarning.Text = "";
            lbNameWarning.Text = "";
            lbPasswordWarning.Text = "";
            lbPhoneWarning.Text = "";
            lbUsernameWarning.Text = "";
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            clearAllWarning();
            try
            {
                if (validateAllFields())
                {
                    Account account = new Account();
                    account.Username = txtUsername.Text.Trim();
                    account.Password = txtPassword.Text;
                    account.Birthday = dtpBirthday.Value.Date;
                    account.Email = txtEmail.Text.Trim();
                    account.Address = txtAddress.Text.Trim();
                    account.Phone = txtPhone.Text.Trim();
                    account.Name = txtName.Text.Trim();
                    AccountDAO dao = new AccountDAO();
                    bool result = dao.addNewStaff(account);
                    if (result)
                        MessageBox.Show("Add new staff successfully!");
                    else MessageBox.Show("Something went wrong, please try again latter!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private bool validateAllFields()
        {
            bool result = true;
            if (txtUsername.Text.Trim() == "") { lbUsernameWarning.Text = "This field must be filled!"; result = false; }
            if (txtPassword.Text == "") { lbPasswordWarning.Text = "This field must be filled!"; result = false; }
            if (txtName.Text.Trim() == "") { lbNameWarning.Text = "This field must be filled!"; result = false; }
            if (txtPhone.Text.Trim() != "" && !Regex.IsMatch(txtPhone.Text.Trim(), "^\\d{10,11}$"))
            {
                lbPhoneWarning.Text = "The phone data format must be 10 - 11 digits!";
                result = false;
            }

            if (txtEmail.Text.Trim() != "" &&
                !Regex.IsMatch(txtEmail.Text.Trim(), @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                lbEmailWarning.Text = "Wrong mail data format";
                result = false;
            }

            return result;
        }

        private void lbPassword_Click(object sender, EventArgs e)
        {

        }
    }
}
