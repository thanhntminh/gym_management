﻿using BussinessObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminControls.Bussiness
{
    public partial class frmResetStaffPassword : Form
    {
        string username;
        public frmResetStaffPassword(string username)
        {
            InitializeComponent();
            this.username = username;
            txtUsername.Text = username;
            clearAllWarning();
        }
        public void clearAllWarning()
        {
            lbWarningConfirm.Text = "";
            lbWarningPassword.Text = "";
        }
        public Boolean ValidateAllField()
        {
            Boolean result = true;
            if (txtPassword.Text == "")
            {
                lbWarningPassword.Text = "This field must be filled!";
                result = false;
            }
            else
            if (txtConfirm.Text != txtPassword.Text)
            {
                lbWarningConfirm.Text = "Confirm password does not match!";
                result = false;
            }
            return result;
        }
        private void btnReset_Click(object sender, EventArgs e)
        {
            clearAllWarning();
            if (ValidateAllField())
            {
                try
                {
                    AccountDAO dao = new AccountDAO();
                    Boolean rs = dao.ResetPassword(username, txtPassword.Text);
                    if (rs)
                    {
                        MessageBox.Show("Reset password successfully!");
                        this.Close();
                    }
                    else MessageBox.Show("Something went wrong! Please try later!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
