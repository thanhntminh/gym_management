﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject.DAO;
using UserControlProject.DTO;
using BussinessObject;

namespace AdminControls.Bussiness
{
    public partial class ucAddNewEquipment : UserControl
    {
        DataTable dt;
        public ucAddNewEquipment()
        {
            InitializeComponent();
            try
            {
                LoadType();
                clearAllWarning();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void LoadType()
        {
            EquipmentTypeDAO dao = new EquipmentTypeDAO();
            dt = dao.GetAllActiveTypes();
            cbType.ValueMember = "id";
            cbType.DisplayMember = "type";
            cbType.DataSource = dt;
        }
        public void clearAllWarning()
        {
            lbWarningBrand.Text = "";
            lbWarningPrice.Text = "";
            lbWarningQuantity.Text = "";
        }
        public Boolean Validate()
        {
            Boolean result = true;
            if (txtQuantity.Text.Trim() == "") { lbWarningQuantity.Text = "This field must be filled!"; result = false; }
            if (txtBrand.Text.Trim() == "") { lbWarningBrand.Text = "This field must be filled!"; result = false; }
            try
            {
                int.Parse(txtQuantity.Text.Trim());
            }
            catch (Exception)
            {
                lbWarningQuantity.Text = "The data format of this field must be digit!"; result = false;
            }
            try
            {
                if (txtPrice.Text.Trim() != "") float.Parse(txtPrice.Text.Trim());
            }
            catch (Exception)
            {
                lbWarningPrice.Text = "The data format of this field must be float!"; result = false;
            }
            return result;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int typeId = int.Parse(cbType.SelectedValue.ToString());
            clearAllWarning();
            if (Validate())
            {
                try
                {
                    EquipmentDAO dao = new EquipmentDAO();
                    Equipment dto = new Equipment();
                    dto.brand = txtBrand.Text.Trim();
                    dto.type = typeId;
                    dto.price = float.Parse(txtPrice.Text.Trim());
                    Boolean result = dao.AddNewEquipment(dto, int.Parse(txtQuantity.Text.Trim()));
                    if (result)
                    {
                        MessageBox.Show("Add new Type successfully!");
                    }
                    else { MessageBox.Show("Add new Type fail!"); }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtBrand.Text = "";
            txtPrice.Text = "";
            txtQuantity.Text = "";
        }

        private void btnRefreshType_Click(object sender, EventArgs e)
        {
            try
            {
                LoadType();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
