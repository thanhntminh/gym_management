﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;

namespace AdminControls.Bussiness
{
    public partial class ucChangeAdminPassword : UserControl
    {
        public ucChangeAdminPassword()
        {
            InitializeComponent();
            clearAllWarning();
        }
        public void clearAllWarning()
        {
            lbWarningConfirm.Text = "";
            lbWarningPassword.Text = "";
        }
        public Boolean ValidateAllField()
        {
            Boolean result = true;
            if (txtPassword.Text == "")
            {
                lbWarningPassword.Text = "This field must be filled!";
                result = false;
            }
            else
            if (txtConfirm.Text != txtPassword.Text)
            {
                lbWarningConfirm.Text = "Confirm password does not match!";
                result = false;
            }
            return result;
        }
        private void btnReset_Click(object sender, EventArgs e)
        {
            clearAllWarning();
            if (ValidateAllField())
            {
                try
                {
                    AccountDAO dao = new AccountDAO();
                    Boolean rs = dao.ResetAdminPassword(txtPassword.Text);
                    if (rs)
                    {
                        MessageBox.Show("Reset password successfully!");
                        txtPassword.Text = "";
                        txtConfirm.Text = "";
                    }
                    else MessageBox.Show("Something went wrong! Please try later!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
