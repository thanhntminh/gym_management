﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject.DAO;

namespace AdminControls.Bussiness
{
    public partial class ucEquipmentType : UserControl
    {
        DataTable dt = null;
        public ucEquipmentType()
        {
            InitializeComponent();
        }

        private void getAllEquipmentTypesToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                EquipmentTypeDAO dao = new EquipmentTypeDAO();
                dt = dao.GetAllTypes();
                dgvEquipmentType.DataSource = dt;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string newType = txtNewType.Text;
                if (newType != null && newType != "")
                {
                    EquipmentTypeDAO dao = new EquipmentTypeDAO();
                    if (dao.addNewType(newType))
                    {
                        getAllEquipmentTypesToolStripButton_Click(null, null);
                        MessageBox.Show("Insert new type successfully!");
                    }
                    else MessageBox.Show("Insert new type fail!");
                }
                else
                {
                    MessageBox.Show("New type text box must be filled!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDisable_Click(object sender, EventArgs e)
        {
            Int32 selectedRowCount = dgvEquipmentType.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (selectedRowCount > 0)
            {
                try
                {
                    EquipmentTypeDAO dao = new EquipmentTypeDAO();
                    for (int i = 0; i < selectedRowCount; i++)
                    {
                        string tmp = dgvEquipmentType.SelectedRows[i].Cells[0].Value.ToString();
                        dao.Disable(int.Parse(tmp));
                    }
                    getAllEquipmentTypesToolStripButton_Click(null, null);
                    MessageBox.Show($"Disable {selectedRowCount} row(s) successfully!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }   
            }
        }

        private void btnEnable_Click(object sender, EventArgs e)
        {
            Int32 selectedRowCount = dgvEquipmentType.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (selectedRowCount > 0)
            {
                try
                {
                    EquipmentTypeDAO dao = new EquipmentTypeDAO();
                    for (int i = 0; i < selectedRowCount; i++)
                    {
                        string tmp = dgvEquipmentType.SelectedRows[i].Cells[0].Value.ToString();
                        dao.Active(int.Parse(tmp));
                    }
                    getAllEquipmentTypesToolStripButton_Click(null, null);
                    MessageBox.Show($"Active {selectedRowCount} row(s) successfully!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }
    }
}
