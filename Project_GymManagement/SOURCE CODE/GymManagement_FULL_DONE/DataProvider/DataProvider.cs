﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DataProvider
{
    public class DataProvider
    {
        public static string getConnectionString()
        {
            string strConnection = ConfigurationManager.ConnectionStrings["GymManagement"].ConnectionString;
            return strConnection;
        }



        public static int ExecuteNonQuery(string query, object[] parameter = null)
        {
            int data = 0;
            using (SqlConnection conn = new SqlConnection(getConnectionString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(query, conn);
                    if (parameter != null)
                    {
                        string[] listParameter = query.Split(' ');
                        int i = 0;
                        foreach (string item in listParameter)
                        {
                            if (item.Contains('@'))
                            {
                                cmd.Parameters.AddWithValue(item, parameter[i]);
                                i++;
                            }
                        }
                    }
                    data = cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    throw new Exception("Error : " + e.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return data;
        }



        public static DataTable ExecuteQuery(string query, object[] parameter = null)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(getConnectionString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(query, conn);
                    if (parameter != null)
                    {
                        string[] listParameter = query.Split(' ');
                        int i = 0;
                        foreach (string item in listParameter)
                        {
                            if (item.Contains('@'))
                            {
                                cmd.Parameters.AddWithValue(item, parameter[i]);
                                i++;
                            }
                        }
                    }
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(data);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return data;
        }
    }
}
