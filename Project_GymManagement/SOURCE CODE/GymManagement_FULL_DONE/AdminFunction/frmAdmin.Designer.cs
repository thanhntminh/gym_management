﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace AdminFunction
{
    partial class frmAdmin
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        private void mtbAccount_DrawItem(Object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush _textBrush;

            // Get the item from the collection.
            TabPage _tabPage = mtbAccount.TabPages[e.Index];

            // Get the real bounds for the tab rectangle.
            Rectangle _tabBounds = mtbAccount.GetTabRect(e.Index);

            if (e.State == DrawItemState.Selected)
            {

                // Draw a different background color, and don't paint a focus rectangle.
                _textBrush = new SolidBrush(Color.Black);
                g.FillRectangle(Brushes.White, e.Bounds);
            }
            else
            {
                _textBrush = new System.Drawing.SolidBrush(e.ForeColor);
                e.DrawBackground();
            }

            // Use our own font.
            Font _tabFont = new Font("Arial", (float)12.0, FontStyle.Bold, GraphicsUnit.Pixel);

            // Draw string. Center the text.
            StringFormat _stringFlags = new StringFormat();
            _stringFlags.Alignment = StringAlignment.Center;
            _stringFlags.LineAlignment = StringAlignment.Center;
            g.DrawString(_tabPage.Text, _tabFont, _textBrush, _tabBounds, new StringFormat(_stringFlags));
        }
        private void mtbEquipment_DrawItem(Object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush _textBrush;

            // Get the item from the collection.
            TabPage _tabPage = mtbEquipment.TabPages[e.Index];

            // Get the real bounds for the tab rectangle.
            Rectangle _tabBounds = mtbEquipment.GetTabRect(e.Index);

            if (e.State == DrawItemState.Selected)
            {

                // Draw a different background color, and don't paint a focus rectangle.
                _textBrush = new SolidBrush(Color.Black);
                g.FillRectangle(Brushes.White, e.Bounds);
            }
            else
            {
                _textBrush = new System.Drawing.SolidBrush(e.ForeColor);
                e.DrawBackground();
            }

            // Use our own font.
            Font _tabFont = new Font("Arial", (float)12.0, FontStyle.Bold, GraphicsUnit.Pixel);

            // Draw string. Center the text.
            StringFormat _stringFlags = new StringFormat();
            _stringFlags.Alignment = StringAlignment.Center;
            _stringFlags.LineAlignment = StringAlignment.Center;
            g.DrawString(_tabPage.Text, _tabFont, _textBrush, _tabBounds, new StringFormat(_stringFlags));
        }
        private void mtbStatistic_DrawItem(Object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush _textBrush;

            // Get the item from the collection.
            TabPage _tabPage = mtbStatistic.TabPages[e.Index];

            // Get the real bounds for the tab rectangle.
            Rectangle _tabBounds = mtbStatistic.GetTabRect(e.Index);

            if (e.State == DrawItemState.Selected)
            {

                // Draw a different background color, and don't paint a focus rectangle.
                _textBrush = new SolidBrush(Color.Black);
                g.FillRectangle(Brushes.White, e.Bounds);
            }
            else
            {
                _textBrush = new System.Drawing.SolidBrush(e.ForeColor);
                e.DrawBackground();
            }

            // Use our own font.
            Font _tabFont = new Font("Arial", (float)12.0, FontStyle.Bold, GraphicsUnit.Pixel);

            // Draw string. Center the text.
            StringFormat _stringFlags = new StringFormat();
            _stringFlags.Alignment = StringAlignment.Center;
            _stringFlags.LineAlignment = StringAlignment.Center;
            g.DrawString(_tabPage.Text, _tabFont, _textBrush, _tabBounds, new StringFormat(_stringFlags));
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAdmin));
            this.btbMain = new System.Windows.Forms.TabControl();
            this.tbAccount = new System.Windows.Forms.TabPage();
            this.mtbAccount = new System.Windows.Forms.TabControl();
            this.mpgAccountAll = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ucAccountList1 = new AdminFunction.Bussiness.ucAccountList();
            this.mpgAccountAddNew = new System.Windows.Forms.TabPage();
            this.ucAccountAddNew1 = new AdminControls.Bussiness.ucAccountAddNew();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.tbEquipment = new System.Windows.Forms.TabPage();
            this.mtbEquipment = new System.Windows.Forms.TabControl();
            this.mpgEquipmentType = new System.Windows.Forms.TabPage();
            this.ucEquipmentType1 = new AdminControls.Bussiness.ucEquipmentType();
            this.mpgEquipmentAll = new System.Windows.Forms.TabPage();
            this.ucEquipmentList1 = new AdminControls.Bussiness.ucEquipmentList();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ucAddNewEquipment1 = new AdminControls.Bussiness.ucAddNewEquipment();
            this.tbStatistic = new System.Windows.Forms.TabPage();
            this.mtbStatistic = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ucMonthlyRevenue1 = new AdminControls.Bussiness.ucMonthlyRevenue();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.memberInformation1 = new UserControlProject.MemberInformation();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.memberHistory1 = new UserControlProject.MemberHistory();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.trainingSession1 = new UserControlProject.TrainingSession();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.ucChangeAdminPassword1 = new AdminControls.Bussiness.ucChangeAdminPassword();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.btbMain.SuspendLayout();
            this.tbAccount.SuspendLayout();
            this.mtbAccount.SuspendLayout();
            this.mpgAccountAll.SuspendLayout();
            this.panel1.SuspendLayout();
            this.mpgAccountAddNew.SuspendLayout();
            this.tbEquipment.SuspendLayout();
            this.mtbEquipment.SuspendLayout();
            this.mpgEquipmentType.SuspendLayout();
            this.mpgEquipmentAll.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tbStatistic.SuspendLayout();
            this.mtbStatistic.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.SuspendLayout();
            // 
            // btbMain
            // 
            this.btbMain.Controls.Add(this.tbAccount);
            this.btbMain.Controls.Add(this.tbEquipment);
            this.btbMain.Controls.Add(this.tbStatistic);
            this.btbMain.Controls.Add(this.tabPage6);
            this.btbMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btbMain.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btbMain.ImageList = this.imageList1;
            this.btbMain.ItemSize = new System.Drawing.Size(200, 50);
            this.btbMain.Location = new System.Drawing.Point(0, 0);
            this.btbMain.Name = "btbMain";
            this.btbMain.SelectedIndex = 0;
            this.btbMain.Size = new System.Drawing.Size(1689, 836);
            this.btbMain.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.btbMain.TabIndex = 0;
            // 
            // tbAccount
            // 
            this.tbAccount.BackColor = System.Drawing.Color.White;
            this.tbAccount.Controls.Add(this.mtbAccount);
            this.tbAccount.ImageIndex = 0;
            this.tbAccount.Location = new System.Drawing.Point(4, 54);
            this.tbAccount.Name = "tbAccount";
            this.tbAccount.Padding = new System.Windows.Forms.Padding(3);
            this.tbAccount.Size = new System.Drawing.Size(1681, 778);
            this.tbAccount.TabIndex = 0;
            this.tbAccount.Text = "Staff";
            // 
            // mtbAccount
            // 
            this.mtbAccount.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.mtbAccount.Controls.Add(this.mpgAccountAll);
            this.mtbAccount.Controls.Add(this.mpgAccountAddNew);
            this.mtbAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mtbAccount.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.mtbAccount.ImageList = this.imageList1;
            this.mtbAccount.ItemSize = new System.Drawing.Size(50, 140);
            this.mtbAccount.Location = new System.Drawing.Point(3, 3);
            this.mtbAccount.Multiline = true;
            this.mtbAccount.Name = "mtbAccount";
            this.mtbAccount.SelectedIndex = 0;
            this.mtbAccount.Size = new System.Drawing.Size(1675, 772);
            this.mtbAccount.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.mtbAccount.TabIndex = 0;
            // 
            // mpgAccountAll
            // 
            this.mpgAccountAll.AutoScroll = true;
            this.mpgAccountAll.BackColor = System.Drawing.Color.White;
            this.mpgAccountAll.Controls.Add(this.panel1);
            this.mpgAccountAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mpgAccountAll.Location = new System.Drawing.Point(144, 4);
            this.mpgAccountAll.Name = "mpgAccountAll";
            this.mpgAccountAll.Padding = new System.Windows.Forms.Padding(3);
            this.mpgAccountAll.Size = new System.Drawing.Size(1527, 764);
            this.mpgAccountAll.TabIndex = 0;
            this.mpgAccountAll.Text = "Staff List";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ucAccountList1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1521, 758);
            this.panel1.TabIndex = 0;
            // 
            // ucAccountList1
            // 
            this.ucAccountList1.BackColor = System.Drawing.Color.White;
            this.ucAccountList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucAccountList1.Location = new System.Drawing.Point(0, 0);
            this.ucAccountList1.Margin = new System.Windows.Forms.Padding(0);
            this.ucAccountList1.Name = "ucAccountList1";
            this.ucAccountList1.Size = new System.Drawing.Size(1521, 758);
            this.ucAccountList1.TabIndex = 0;
            this.ucAccountList1.Load += new System.EventHandler(this.ucAccountList1_Load);
            // 
            // mpgAccountAddNew
            // 
            this.mpgAccountAddNew.AutoScroll = true;
            this.mpgAccountAddNew.BackColor = System.Drawing.Color.White;
            this.mpgAccountAddNew.Controls.Add(this.ucAccountAddNew1);
            this.mpgAccountAddNew.ImageKey = "(none)";
            this.mpgAccountAddNew.Location = new System.Drawing.Point(144, 4);
            this.mpgAccountAddNew.Name = "mpgAccountAddNew";
            this.mpgAccountAddNew.Padding = new System.Windows.Forms.Padding(3);
            this.mpgAccountAddNew.Size = new System.Drawing.Size(1527, 764);
            this.mpgAccountAddNew.TabIndex = 1;
            this.mpgAccountAddNew.Text = "Add new staff";
            this.mpgAccountAddNew.Click += new System.EventHandler(this.tabAddNewAccount_Click);
            // 
            // ucAccountAddNew1
            // 
            this.ucAccountAddNew1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucAccountAddNew1.Location = new System.Drawing.Point(3, 3);
            this.ucAccountAddNew1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.ucAccountAddNew1.Name = "ucAccountAddNew1";
            this.ucAccountAddNew1.Size = new System.Drawing.Size(1500, 846);
            this.ucAccountAddNew1.TabIndex = 0;
            this.ucAccountAddNew1.Load += new System.EventHandler(this.ucAccountAddNew1_Load);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "User_medium");
            this.imageList1.Images.SetKeyName(1, "Equipment");
            this.imageList1.Images.SetKeyName(2, "statistic");
            this.imageList1.Images.SetKeyName(3, "add");
            this.imageList1.Images.SetKeyName(4, "user-add");
            this.imageList1.Images.SetKeyName(5, "Stepper");
            this.imageList1.Images.SetKeyName(6, "Reset-48.png");
            // 
            // tbEquipment
            // 
            this.tbEquipment.BackColor = System.Drawing.Color.White;
            this.tbEquipment.Controls.Add(this.mtbEquipment);
            this.tbEquipment.ImageIndex = 5;
            this.tbEquipment.Location = new System.Drawing.Point(4, 54);
            this.tbEquipment.Name = "tbEquipment";
            this.tbEquipment.Padding = new System.Windows.Forms.Padding(3);
            this.tbEquipment.Size = new System.Drawing.Size(1681, 778);
            this.tbEquipment.TabIndex = 1;
            this.tbEquipment.Text = "Equipment";
            this.tbEquipment.UseVisualStyleBackColor = true;
            // 
            // mtbEquipment
            // 
            this.mtbEquipment.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.mtbEquipment.Controls.Add(this.mpgEquipmentType);
            this.mtbEquipment.Controls.Add(this.mpgEquipmentAll);
            this.mtbEquipment.Controls.Add(this.tabPage1);
            this.mtbEquipment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mtbEquipment.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.mtbEquipment.ItemSize = new System.Drawing.Size(50, 140);
            this.mtbEquipment.Location = new System.Drawing.Point(3, 3);
            this.mtbEquipment.Multiline = true;
            this.mtbEquipment.Name = "mtbEquipment";
            this.mtbEquipment.SelectedIndex = 0;
            this.mtbEquipment.Size = new System.Drawing.Size(1675, 772);
            this.mtbEquipment.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.mtbEquipment.TabIndex = 1;
            // 
            // mpgEquipmentType
            // 
            this.mpgEquipmentType.BackColor = System.Drawing.Color.White;
            this.mpgEquipmentType.Controls.Add(this.ucEquipmentType1);
            this.mpgEquipmentType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mpgEquipmentType.Location = new System.Drawing.Point(144, 4);
            this.mpgEquipmentType.Name = "mpgEquipmentType";
            this.mpgEquipmentType.Padding = new System.Windows.Forms.Padding(3);
            this.mpgEquipmentType.Size = new System.Drawing.Size(1527, 764);
            this.mpgEquipmentType.TabIndex = 0;
            this.mpgEquipmentType.Text = "Equipment Type";
            this.mpgEquipmentType.UseVisualStyleBackColor = true;
            // 
            // ucEquipmentType1
            // 
            this.ucEquipmentType1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucEquipmentType1.Location = new System.Drawing.Point(3, 3);
            this.ucEquipmentType1.Margin = new System.Windows.Forms.Padding(4);
            this.ucEquipmentType1.Name = "ucEquipmentType1";
            this.ucEquipmentType1.Size = new System.Drawing.Size(1521, 758);
            this.ucEquipmentType1.TabIndex = 0;
            // 
            // mpgEquipmentAll
            // 
            this.mpgEquipmentAll.BackColor = System.Drawing.Color.White;
            this.mpgEquipmentAll.Controls.Add(this.ucEquipmentList1);
            this.mpgEquipmentAll.Location = new System.Drawing.Point(144, 4);
            this.mpgEquipmentAll.Name = "mpgEquipmentAll";
            this.mpgEquipmentAll.Padding = new System.Windows.Forms.Padding(3);
            this.mpgEquipmentAll.Size = new System.Drawing.Size(1527, 764);
            this.mpgEquipmentAll.TabIndex = 1;
            this.mpgEquipmentAll.Text = "Equipment List";
            this.mpgEquipmentAll.UseVisualStyleBackColor = true;
            // 
            // ucEquipmentList1
            // 
            this.ucEquipmentList1.AutoSize = true;
            this.ucEquipmentList1.Location = new System.Drawing.Point(3, 3);
            this.ucEquipmentList1.Margin = new System.Windows.Forms.Padding(4);
            this.ucEquipmentList1.Name = "ucEquipmentList1";
            this.ucEquipmentList1.Size = new System.Drawing.Size(1976, 831);
            this.ucEquipmentList1.TabIndex = 0;
            this.ucEquipmentList1.Load += new System.EventHandler(this.ucEquipmentList1_Load);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ucAddNewEquipment1);
            this.tabPage1.Location = new System.Drawing.Point(144, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1527, 764);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Add New Equipment";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ucAddNewEquipment1
            // 
            this.ucAddNewEquipment1.Location = new System.Drawing.Point(3, 3);
            this.ucAddNewEquipment1.Margin = new System.Windows.Forms.Padding(4);
            this.ucAddNewEquipment1.Name = "ucAddNewEquipment1";
            this.ucAddNewEquipment1.Size = new System.Drawing.Size(1116, 613);
            this.ucAddNewEquipment1.TabIndex = 0;
            // 
            // tbStatistic
            // 
            this.tbStatistic.BackColor = System.Drawing.Color.White;
            this.tbStatistic.Controls.Add(this.mtbStatistic);
            this.tbStatistic.ImageIndex = 2;
            this.tbStatistic.Location = new System.Drawing.Point(4, 54);
            this.tbStatistic.Name = "tbStatistic";
            this.tbStatistic.Padding = new System.Windows.Forms.Padding(3);
            this.tbStatistic.Size = new System.Drawing.Size(1681, 778);
            this.tbStatistic.TabIndex = 2;
            this.tbStatistic.Text = "Statistic";
            this.tbStatistic.UseVisualStyleBackColor = true;
            // 
            // mtbStatistic
            // 
            this.mtbStatistic.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.mtbStatistic.Controls.Add(this.tabPage2);
            this.mtbStatistic.Controls.Add(this.tabPage3);
            this.mtbStatistic.Controls.Add(this.tabPage4);
            this.mtbStatistic.Controls.Add(this.tabPage5);
            this.mtbStatistic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mtbStatistic.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.mtbStatistic.ItemSize = new System.Drawing.Size(50, 140);
            this.mtbStatistic.Location = new System.Drawing.Point(3, 3);
            this.mtbStatistic.Multiline = true;
            this.mtbStatistic.Name = "mtbStatistic";
            this.mtbStatistic.SelectedIndex = 0;
            this.mtbStatistic.Size = new System.Drawing.Size(1675, 772);
            this.mtbStatistic.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.mtbStatistic.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ucMonthlyRevenue1);
            this.tabPage2.Location = new System.Drawing.Point(144, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1527, 764);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "Revenue";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ucMonthlyRevenue1
            // 
            this.ucMonthlyRevenue1.Location = new System.Drawing.Point(250, 86);
            this.ucMonthlyRevenue1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ucMonthlyRevenue1.Name = "ucMonthlyRevenue1";
            this.ucMonthlyRevenue1.Size = new System.Drawing.Size(1135, 530);
            this.ucMonthlyRevenue1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.memberInformation1);
            this.tabPage3.Location = new System.Drawing.Point(144, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1527, 764);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "Member List";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // memberInformation1
            // 
            this.memberInformation1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memberInformation1.Location = new System.Drawing.Point(3, 3);
            this.memberInformation1.Margin = new System.Windows.Forms.Padding(5);
            this.memberInformation1.Name = "memberInformation1";
            this.memberInformation1.Size = new System.Drawing.Size(1521, 758);
            this.memberInformation1.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.memberHistory1);
            this.tabPage4.Location = new System.Drawing.Point(144, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1527, 764);
            this.tabPage4.TabIndex = 2;
            this.tabPage4.Text = "Member History";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // memberHistory1
            // 
            this.memberHistory1.AutoSize = true;
            this.memberHistory1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memberHistory1.Location = new System.Drawing.Point(0, 0);
            this.memberHistory1.Margin = new System.Windows.Forms.Padding(5);
            this.memberHistory1.Name = "memberHistory1";
            this.memberHistory1.Size = new System.Drawing.Size(1527, 764);
            this.memberHistory1.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.trainingSession1);
            this.tabPage5.Location = new System.Drawing.Point(144, 4);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1527, 764);
            this.tabPage5.TabIndex = 3;
            this.tabPage5.Text = "Month Traning Sessions History";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // trainingSession1
            // 
            this.trainingSession1.AutoSize = true;
            this.trainingSession1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trainingSession1.Location = new System.Drawing.Point(0, 0);
            this.trainingSession1.Margin = new System.Windows.Forms.Padding(5);
            this.trainingSession1.Name = "trainingSession1";
            this.trainingSession1.Size = new System.Drawing.Size(1527, 764);
            this.trainingSession1.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.ucChangeAdminPassword1);
            this.tabPage6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage6.ImageIndex = 6;
            this.tabPage6.Location = new System.Drawing.Point(4, 54);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1681, 778);
            this.tabPage6.TabIndex = 3;
            this.tabPage6.Text = "Change Password";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // ucChangeAdminPassword1
            // 
            this.ucChangeAdminPassword1.Location = new System.Drawing.Point(523, 145);
            this.ucChangeAdminPassword1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ucChangeAdminPassword1.Name = "ucChangeAdminPassword1";
            this.ucChangeAdminPassword1.Size = new System.Drawing.Size(561, 295);
            this.ucChangeAdminPassword1.TabIndex = 0;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "gym.png");
            // 
            // frmAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1689, 836);
            this.Controls.Add(this.btbMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimumSize = new System.Drawing.Size(1707, 832);
            this.Name = "frmAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.btbMain.ResumeLayout(false);
            this.tbAccount.ResumeLayout(false);
            this.mtbAccount.ResumeLayout(false);
            this.mpgAccountAll.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.mpgAccountAddNew.ResumeLayout(false);
            this.tbEquipment.ResumeLayout(false);
            this.mtbEquipment.ResumeLayout(false);
            this.mpgEquipmentType.ResumeLayout(false);
            this.mpgEquipmentAll.ResumeLayout(false);
            this.mpgEquipmentAll.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tbStatistic.ResumeLayout(false);
            this.mtbStatistic.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl btbMain;
        private System.Windows.Forms.TabPage tbAccount;
        private System.Windows.Forms.TabPage tbEquipment;
        private System.Windows.Forms.TabControl mtbAccount;
        private System.Windows.Forms.TabPage mpgAccountAll;
        private System.Windows.Forms.TabPage tbStatistic;
        private TabControl mtbEquipment;
        private TabPage mpgEquipmentType;
        private TabPage mpgEquipmentAll;
        private TabPage tabPage1;
        public ImageList imageList1;
        private ImageList imageList2;
        private TabPage mpgAccountAddNew;
        private Panel panel1;

        private AdminControls.Bussiness.ucEquipmentType ucEquipmentType1;
        private AdminControls.Bussiness.ucEquipmentList ucEquipmentList1;
        private AdminControls.Bussiness.ucAddNewEquipment ucAddNewEquipment1;
        private TabControl mtbStatistic;
        private TabPage tabPage2;
        private TabPage tabPage3;
        private TabPage tabPage4;
        private TabPage tabPage5;
        private Bussiness.ucAccountList ucAccountList1;
        private AdminControls.Bussiness.ucAccountAddNew ucAccountAddNew1;
        private TabPage tabPage6;
        private AdminControls.Bussiness.ucChangeAdminPassword ucChangeAdminPassword1;
        private UserControlProject.MemberInformation memberInformation1;
        private AdminControls.Bussiness.ucMonthlyRevenue ucMonthlyRevenue1;
        private UserControlProject.MemberHistory memberHistory1;
        private UserControlProject.TrainingSession trainingSession1;
    }
}

