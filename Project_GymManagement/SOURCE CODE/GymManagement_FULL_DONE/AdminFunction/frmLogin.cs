﻿using System;
using System.Threading;
using System.Windows.Forms;
using BussinessObject;
using GymnasiaManagement;
using UserControlProject;
using System.Drawing;

namespace AdminFunction
{
    public partial class frmLogin : Form
    {
        Account account = new Account();

        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            bool check = true;

            BussinessObject.AccountDAO dao = new BussinessObject.AccountDAO();

            if (String.IsNullOrEmpty(txtUsername.Text) || txtUsername.Text.Equals("USERNAME"))
            {
                lbUsername.ForeColor = Color.Red;
                lbUsername.Text = "Username can't be blank!";
                check = false;
            }
            else
            {
                lbUsername.Text = "";
            }

            if (String.IsNullOrEmpty(txtPassword.Text) || txtPassword.Text.Equals("PASSWORD"))
            {
                lbPassword.ForeColor = Color.Red;
                lbPassword.Text = "Password can't be blank!";
                check = false;
            }
            else
            {
                lbPassword.Text = "";
            }

            if (check)
            {
                bool checkLogin = false;

                account = dao.CheckLogin(txtUsername.Text, txtPassword.Text);
                if (account == null)
                {
                    lbInvalid.ForeColor = Color.Red;
                    lbInvalid.Text = "Invalid Username or Password!";
                    txtUsername.Focus();
                    checkLogin = false;
                }
                else
                {
                    lbInvalid.Text = "";
                    checkLogin = true;
                }
                if (checkLogin)
                {
                    if (account.Role.Equals(Account.ROLE_ADMIN))
                    {
                        this.Hide();
                        Thread t = new Thread(new ThreadStart(ShowFormAdmin));
                        t.Start();
                        this.Close();
                    }
                    else if (account.Role.Equals(Account.ROLE_STAFF))
                    {
                        Session.CurrentUser = account;
                        Session.CurrentUser.Password = txtPassword.Text;
                        Session.CurrentUser.Username = txtUsername.Text;
                        this.Hide();
                        Thread t = new Thread(new ThreadStart(ShowFormStaff));
                        t.Start();
                        this.Close();
                    }
                }
            }
        }
        private void ShowFormAdmin()
        {
            frmAdmin fAdmin = new frmAdmin();
            Application.Run(fAdmin);
        }

        private void ShowFormStaff()
        {
            frmStaffs fStaff = new frmStaffs(account);
            Application.Run(fStaff);
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
