﻿namespace GymnasiaManagement
{
    partial class frmStaffs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStaffs));
            this.frmStaff = new System.Windows.Forms.TabControl();
            this.tpMember = new System.Windows.Forms.TabPage();
            this.tcMember = new System.Windows.Forms.TabControl();
            this.tpAddMember = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tpMemberInfo = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tpAttendance = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tpGuestAttendance = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.CheckAttendanceV2 = new System.Windows.Forms.TabPage();
            this.tbExtendDay = new System.Windows.Forms.TabPage();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.tcEquipments = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tcStatistics = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tcAccount = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.addNewMember1 = new UserControlProject.AddNewMember();
            this.memberInformation1 = new UserControlProject.MemberInformation();
            this.checkAttendanceMembers1 = new UserControlProject.CheckAttendanceMembers();
            this.checkAttendanceGuest1 = new UserControlProject.CheckAttendanceGuest();
            this.extendDay1 = new UserControlProject.ExtendDay();
            this.viewEquipment1 = new UserControlProject.ViewEquipment();
            this.numberOfMembers1 = new UserControlProject.NumberOfMembers();
            this.memberHistory1 = new UserControlProject.MemberHistory();
            this.trainingSession1 = new UserControlProject.TrainingSession();
            this.changePassword1 = new UserControlProject.ChangePassword();
            this.checkAttendanceByCode3 = new UserControlProject.CheckAttendanceByCode();
            this.frmStaff.SuspendLayout();
            this.tpMember.SuspendLayout();
            this.tcMember.SuspendLayout();
            this.tpAddMember.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tpMemberInfo.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tpAttendance.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tpGuestAttendance.SuspendLayout();
            this.panel9.SuspendLayout();
            this.CheckAttendanceV2.SuspendLayout();
            this.tbExtendDay.SuspendLayout();
            this.tcEquipments.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tcStatistics.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tcAccount.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // frmStaff
            // 
            this.frmStaff.Controls.Add(this.tpMember);
            this.frmStaff.Controls.Add(this.tcEquipments);
            this.frmStaff.Controls.Add(this.tabPage3);
            this.frmStaff.Controls.Add(this.tabPage4);
            this.frmStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frmStaff.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.frmStaff.ImageList = this.imageList1;
            this.frmStaff.ItemSize = new System.Drawing.Size(140, 50);
            this.frmStaff.Location = new System.Drawing.Point(0, 0);
            this.frmStaff.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.frmStaff.Name = "frmStaff";
            this.frmStaff.SelectedIndex = 0;
            this.frmStaff.Size = new System.Drawing.Size(1580, 740);
            this.frmStaff.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.frmStaff.TabIndex = 1;
            // 
            // tpMember
            // 
            this.tpMember.Controls.Add(this.tcMember);
            this.tpMember.ImageIndex = 0;
            this.tpMember.Location = new System.Drawing.Point(4, 54);
            this.tpMember.Margin = new System.Windows.Forms.Padding(4);
            this.tpMember.Name = "tpMember";
            this.tpMember.Padding = new System.Windows.Forms.Padding(4);
            this.tpMember.Size = new System.Drawing.Size(1572, 682);
            this.tpMember.TabIndex = 0;
            this.tpMember.Text = "Members";
            this.tpMember.UseVisualStyleBackColor = true;
            // 
            // tcMember
            // 
            this.tcMember.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tcMember.Controls.Add(this.tpAddMember);
            this.tcMember.Controls.Add(this.tpMemberInfo);
            this.tcMember.Controls.Add(this.tpAttendance);
            this.tcMember.Controls.Add(this.tpGuestAttendance);
            this.tcMember.Controls.Add(this.CheckAttendanceV2);
            this.tcMember.Controls.Add(this.tbExtendDay);
            this.tcMember.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMember.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tcMember.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.tcMember.ImageList = this.imageList2;
            this.tcMember.ItemSize = new System.Drawing.Size(60, 150);
            this.tcMember.Location = new System.Drawing.Point(4, 4);
            this.tcMember.Margin = new System.Windows.Forms.Padding(4);
            this.tcMember.Multiline = true;
            this.tcMember.Name = "tcMember";
            this.tcMember.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tcMember.SelectedIndex = 0;
            this.tcMember.Size = new System.Drawing.Size(1564, 674);
            this.tcMember.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tcMember.TabIndex = 0;
            this.tcMember.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tcMember_DrawItem);
            // 
            // tpAddMember
            // 
            this.tpAddMember.Controls.Add(this.panel1);
            this.tpAddMember.ImageIndex = 0;
            this.tpAddMember.Location = new System.Drawing.Point(154, 4);
            this.tpAddMember.Margin = new System.Windows.Forms.Padding(4);
            this.tpAddMember.Name = "tpAddMember";
            this.tpAddMember.Padding = new System.Windows.Forms.Padding(4);
            this.tpAddMember.Size = new System.Drawing.Size(1406, 666);
            this.tpAddMember.TabIndex = 0;
            this.tpAddMember.Text = "Add new member";
            this.tpAddMember.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.addNewMember1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1398, 658);
            this.panel1.TabIndex = 0;
            // 
            // tpMemberInfo
            // 
            this.tpMemberInfo.Controls.Add(this.panel2);
            this.tpMemberInfo.ImageIndex = 1;
            this.tpMemberInfo.Location = new System.Drawing.Point(154, 4);
            this.tpMemberInfo.Margin = new System.Windows.Forms.Padding(4);
            this.tpMemberInfo.Name = "tpMemberInfo";
            this.tpMemberInfo.Padding = new System.Windows.Forms.Padding(4);
            this.tpMemberInfo.Size = new System.Drawing.Size(1406, 666);
            this.tpMemberInfo.TabIndex = 1;
            this.tpMemberInfo.Text = "Member\'s information";
            this.tpMemberInfo.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.Controls.Add(this.memberInformation1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(4, 4);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1398, 658);
            this.panel2.TabIndex = 0;
            // 
            // tpAttendance
            // 
            this.tpAttendance.Controls.Add(this.panel3);
            this.tpAttendance.ImageIndex = 2;
            this.tpAttendance.Location = new System.Drawing.Point(154, 4);
            this.tpAttendance.Margin = new System.Windows.Forms.Padding(4);
            this.tpAttendance.Name = "tpAttendance";
            this.tpAttendance.Padding = new System.Windows.Forms.Padding(4);
            this.tpAttendance.Size = new System.Drawing.Size(1406, 666);
            this.tpAttendance.TabIndex = 2;
            this.tpAttendance.Text = "Member\'s Attendance";
            this.tpAttendance.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.Controls.Add(this.checkAttendanceMembers1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(4, 4);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1398, 658);
            this.panel3.TabIndex = 0;
            // 
            // tpGuestAttendance
            // 
            this.tpGuestAttendance.Controls.Add(this.panel9);
            this.tpGuestAttendance.Location = new System.Drawing.Point(154, 4);
            this.tpGuestAttendance.Margin = new System.Windows.Forms.Padding(4);
            this.tpGuestAttendance.Name = "tpGuestAttendance";
            this.tpGuestAttendance.Size = new System.Drawing.Size(1406, 666);
            this.tpGuestAttendance.TabIndex = 0;
            this.tpGuestAttendance.Text = "Guest\'s Attendance";
            this.tpGuestAttendance.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.AutoSize = true;
            this.panel9.Controls.Add(this.checkAttendanceGuest1);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Margin = new System.Windows.Forms.Padding(4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1406, 666);
            this.panel9.TabIndex = 0;
            // 
            // CheckAttendanceV2
            // 
            this.CheckAttendanceV2.Controls.Add(this.checkAttendanceByCode3);
            this.CheckAttendanceV2.Location = new System.Drawing.Point(154, 4);
            this.CheckAttendanceV2.Margin = new System.Windows.Forms.Padding(4);
            this.CheckAttendanceV2.Name = "CheckAttendanceV2";
            this.CheckAttendanceV2.Padding = new System.Windows.Forms.Padding(4);
            this.CheckAttendanceV2.Size = new System.Drawing.Size(1406, 666);
            this.CheckAttendanceV2.TabIndex = 3;
            this.CheckAttendanceV2.Text = "Check attendance by code";
            this.CheckAttendanceV2.UseVisualStyleBackColor = true;
            // 
            // tbExtendDay
            // 
            this.tbExtendDay.Controls.Add(this.extendDay1);
            this.tbExtendDay.Location = new System.Drawing.Point(154, 4);
            this.tbExtendDay.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbExtendDay.Name = "tbExtendDay";
            this.tbExtendDay.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbExtendDay.Size = new System.Drawing.Size(1406, 666);
            this.tbExtendDay.TabIndex = 4;
            this.tbExtendDay.Text = "Extend Day";
            this.tbExtendDay.UseVisualStyleBackColor = true;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "icons8-Add User Group Man Woman-64.png");
            this.imageList2.Images.SetKeyName(1, "icons8-Add User Group Man Woman-64.png");
            this.imageList2.Images.SetKeyName(2, "icons8-Add User Group Man Woman-64.png");
            // 
            // tcEquipments
            // 
            this.tcEquipments.Controls.Add(this.panel4);
            this.tcEquipments.ImageIndex = 2;
            this.tcEquipments.Location = new System.Drawing.Point(4, 54);
            this.tcEquipments.Margin = new System.Windows.Forms.Padding(4);
            this.tcEquipments.Name = "tcEquipments";
            this.tcEquipments.Padding = new System.Windows.Forms.Padding(4);
            this.tcEquipments.Size = new System.Drawing.Size(1572, 682);
            this.tcEquipments.TabIndex = 1;
            this.tcEquipments.Text = "Equipments";
            this.tcEquipments.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.viewEquipment1);
            this.panel4.Location = new System.Drawing.Point(8, 7);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1655, 761);
            this.panel4.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tcStatistics);
            this.tabPage3.ImageIndex = 3;
            this.tabPage3.Location = new System.Drawing.Point(4, 54);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage3.Size = new System.Drawing.Size(1572, 682);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Statistics";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tcStatistics
            // 
            this.tcStatistics.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tcStatistics.Controls.Add(this.tabPage2);
            this.tcStatistics.Controls.Add(this.tabPage5);
            this.tcStatistics.Controls.Add(this.tabPage6);
            this.tcStatistics.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcStatistics.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tcStatistics.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.tcStatistics.ItemSize = new System.Drawing.Size(60, 150);
            this.tcStatistics.Location = new System.Drawing.Point(4, 4);
            this.tcStatistics.Margin = new System.Windows.Forms.Padding(4);
            this.tcStatistics.Multiline = true;
            this.tcStatistics.Name = "tcStatistics";
            this.tcStatistics.SelectedIndex = 0;
            this.tcStatistics.Size = new System.Drawing.Size(1564, 674);
            this.tcStatistics.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tcStatistics.TabIndex = 0;
            this.tcStatistics.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tcStatistics_DrawItem);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel6);
            this.tabPage2.Location = new System.Drawing.Point(154, 4);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(1406, 666);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "Number of members";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.AutoSize = true;
            this.panel6.Controls.Add(this.numberOfMembers1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(4, 4);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1398, 658);
            this.panel6.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.panel7);
            this.tabPage5.Location = new System.Drawing.Point(154, 4);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage5.Size = new System.Drawing.Size(1406, 666);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "Member history";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.AutoSize = true;
            this.panel7.Controls.Add(this.memberHistory1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(4, 4);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1398, 658);
            this.panel7.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.panel8);
            this.tabPage6.Location = new System.Drawing.Point(154, 4);
            this.tabPage6.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage6.Size = new System.Drawing.Size(1406, 666);
            this.tabPage6.TabIndex = 2;
            this.tabPage6.Text = "Training Session";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.AutoSize = true;
            this.panel8.Controls.Add(this.trainingSession1);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(4, 4);
            this.panel8.Margin = new System.Windows.Forms.Padding(4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1398, 658);
            this.panel8.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tcAccount);
            this.tabPage4.ImageIndex = 1;
            this.tabPage4.Location = new System.Drawing.Point(4, 54);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage4.Size = new System.Drawing.Size(1572, 682);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Account";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tcAccount
            // 
            this.tcAccount.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tcAccount.Controls.Add(this.tabPage1);
            this.tcAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcAccount.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tcAccount.ItemSize = new System.Drawing.Size(60, 150);
            this.tcAccount.Location = new System.Drawing.Point(4, 4);
            this.tcAccount.Margin = new System.Windows.Forms.Padding(4);
            this.tcAccount.Multiline = true;
            this.tcAccount.Name = "tcAccount";
            this.tcAccount.SelectedIndex = 0;
            this.tcAccount.Size = new System.Drawing.Size(1564, 674);
            this.tcAccount.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tcAccount.TabIndex = 0;
            this.tcAccount.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tcAccount_DrawItem);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.tabPage1.Location = new System.Drawing.Point(154, 4);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(1406, 666);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Change Password";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.AutoSize = true;
            this.panel5.Controls.Add(this.changePassword1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(4, 4);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1398, 658);
            this.panel5.TabIndex = 0;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "icons8-Manager-50.png");
            this.imageList1.Images.SetKeyName(1, "icons8-User Rights-50.png");
            this.imageList1.Images.SetKeyName(2, "icons8-Maintenance-50.png");
            this.imageList1.Images.SetKeyName(3, "icons8-Compose-50.png");
            // 
            // addNewMember1
            // 
            this.addNewMember1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addNewMember1.Location = new System.Drawing.Point(0, 0);
            this.addNewMember1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.addNewMember1.Name = "addNewMember1";
            this.addNewMember1.Size = new System.Drawing.Size(1398, 658);
            this.addNewMember1.TabIndex = 1;
            // 
            // memberInformation1
            // 
            this.memberInformation1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memberInformation1.Location = new System.Drawing.Point(0, 0);
            this.memberInformation1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.memberInformation1.Name = "memberInformation1";
            this.memberInformation1.Size = new System.Drawing.Size(1398, 658);
            this.memberInformation1.TabIndex = 0;
            // 
            // checkAttendanceMembers1
            // 
            this.checkAttendanceMembers1.Location = new System.Drawing.Point(0, 0);
            this.checkAttendanceMembers1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.checkAttendanceMembers1.Name = "checkAttendanceMembers1";
            this.checkAttendanceMembers1.Size = new System.Drawing.Size(1469, 740);
            this.checkAttendanceMembers1.TabIndex = 0;
            // 
            // checkAttendanceGuest1
            // 
            this.checkAttendanceGuest1.Location = new System.Drawing.Point(395, 78);
            this.checkAttendanceGuest1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.checkAttendanceGuest1.Name = "checkAttendanceGuest1";
            this.checkAttendanceGuest1.Size = new System.Drawing.Size(732, 386);
            this.checkAttendanceGuest1.TabIndex = 0;
            // 
            // extendDay1
            // 
            this.extendDay1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.extendDay1.Location = new System.Drawing.Point(3, 2);
            this.extendDay1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.extendDay1.Name = "extendDay1";
            this.extendDay1.Size = new System.Drawing.Size(1400, 662);
            this.extendDay1.TabIndex = 0;
            this.extendDay1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.extendDay1_MouseClick);
            // 
            // viewEquipment1
            // 
            this.viewEquipment1.Font = new System.Drawing.Font("Arial", 12F);
            this.viewEquipment1.Location = new System.Drawing.Point(7, 5);
            this.viewEquipment1.Margin = new System.Windows.Forms.Padding(7, 5, 7, 5);
            this.viewEquipment1.Name = "viewEquipment1";
            this.viewEquipment1.Size = new System.Drawing.Size(1659, 694);
            this.viewEquipment1.TabIndex = 0;
            // 
            // numberOfMembers1
            // 
            this.numberOfMembers1.Location = new System.Drawing.Point(4, 0);
            this.numberOfMembers1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.numberOfMembers1.Name = "numberOfMembers1";
            this.numberOfMembers1.Size = new System.Drawing.Size(1639, 740);
            this.numberOfMembers1.TabIndex = 0;
            // 
            // memberHistory1
            // 
            this.memberHistory1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memberHistory1.Location = new System.Drawing.Point(0, 0);
            this.memberHistory1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.memberHistory1.Name = "memberHistory1";
            this.memberHistory1.Size = new System.Drawing.Size(1398, 658);
            this.memberHistory1.TabIndex = 0;
            // 
            // trainingSession1
            // 
            this.trainingSession1.Location = new System.Drawing.Point(4, 4);
            this.trainingSession1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.trainingSession1.Name = "trainingSession1";
            this.trainingSession1.Size = new System.Drawing.Size(1619, 594);
            this.trainingSession1.TabIndex = 0;
            // 
            // changePassword1
            // 
            this.changePassword1.Location = new System.Drawing.Point(0, 0);
            this.changePassword1.Margin = new System.Windows.Forms.Padding(5);
            this.changePassword1.Name = "changePassword1";
            this.changePassword1.Size = new System.Drawing.Size(1523, 738);
            this.changePassword1.TabIndex = 0;
            // 
            // checkAttendanceByCode3
            // 
            this.checkAttendanceByCode3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkAttendanceByCode3.Location = new System.Drawing.Point(4, 4);
            this.checkAttendanceByCode3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkAttendanceByCode3.Name = "checkAttendanceByCode3";
            this.checkAttendanceByCode3.Size = new System.Drawing.Size(1398, 658);
            this.checkAttendanceByCode3.TabIndex = 0;
            // 
            // frmStaffs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1580, 740);
            this.Controls.Add(this.frmStaff);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmStaffs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Staff";
            this.frmStaff.ResumeLayout(false);
            this.tpMember.ResumeLayout(false);
            this.tcMember.ResumeLayout(false);
            this.tpAddMember.ResumeLayout(false);
            this.tpAddMember.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tpMemberInfo.ResumeLayout(false);
            this.tpMemberInfo.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tpAttendance.ResumeLayout(false);
            this.tpAttendance.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.tpGuestAttendance.ResumeLayout(false);
            this.tpGuestAttendance.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.CheckAttendanceV2.ResumeLayout(false);
            this.tbExtendDay.ResumeLayout(false);
            this.tcEquipments.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tcStatistics.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tcAccount.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl frmStaff;
        private System.Windows.Forms.TabPage tpMember;
        private System.Windows.Forms.TabPage tcEquipments;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        public System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TabControl tcMember;
        private System.Windows.Forms.TabPage tpAddMember;
        private System.Windows.Forms.TabPage tpAttendance;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TabControl tcAccount;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tcStatistics;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.TabPage tpMemberInfo;
        private System.Windows.Forms.Panel panel1;
        private UserControlProject.AddNewMember addNewMember1;
        private System.Windows.Forms.Panel panel2;
        private UserControlProject.MemberInformation memberInformation1;
        private System.Windows.Forms.Panel panel3;
        private UserControlProject.ViewEquipment viewEquipment1;
        private System.Windows.Forms.Panel panel6;
        private UserControlProject.NumberOfMembers numberOfMembers1;
        private System.Windows.Forms.Panel panel7;
        private UserControlProject.MemberHistory memberHistory1;
        private System.Windows.Forms.Panel panel8;
        private UserControlProject.TrainingSession trainingSession1;
        private System.Windows.Forms.Panel panel5;
        private UserControlProject.ChangePassword changePassword1;
        private UserControlProject.CheckAttendanceMembers checkAttendanceMembers1;
        private System.Windows.Forms.TabPage tpGuestAttendance;
        private System.Windows.Forms.Panel panel9;
        private UserControlProject.CheckAttendanceGuest checkAttendanceGuest1;
        private System.Windows.Forms.TabPage CheckAttendanceV2;
        private UserControlProject.CheckAttendanceByCode checkAttendanceByCode1;
        private System.Windows.Forms.TabPage tbExtendDay;
        private UserControlProject.ExtendDay extendDay1;
        private UserControlProject.CheckAttendanceByCode checkAttendanceByCode2;
        private UserControlProject.CheckAttendanceByCode checkAttendanceByCode3;
    }
}

