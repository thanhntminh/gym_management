﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessObject;

namespace AdminFunction
{
    public partial class frmAdmin : Form
    {
        public Account INFO { get; set; }

        public frmAdmin()
        {
            this.WindowState = FormWindowState.Maximized;
            InitializeComponent();
            mtbAccount.DrawItem += new DrawItemEventHandler(mtbAccount_DrawItem);
            mtbEquipment.DrawItem += new DrawItemEventHandler(mtbEquipment_DrawItem);
            mtbStatistic.DrawItem += new DrawItemEventHandler(mtbStatistic_DrawItem);
        }

        private void tabAddNewAccount_Click(object sender, EventArgs e)
        {

        }

        private void frmAccountList1_Load(object sender, EventArgs e)
        {

        }

        private void ucAccountAddNew1_Load(object sender, EventArgs e)
        {

        }

        private void frmMain_Load(object sender, EventArgs e)
        {


        }

        private void ucAccountList1_Load(object sender, EventArgs e)
        {

        }

        private void ucEquipmentList1_Load(object sender, EventArgs e)
        {

        }
    }
}
