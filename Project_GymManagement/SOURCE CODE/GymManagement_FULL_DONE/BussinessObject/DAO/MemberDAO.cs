﻿using BussinessObject.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserControlProject.DTO;

namespace BussinessObject
{

    public class MemberDAO
    {

        public History GetHistory(string memberId)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "SELECT memberId , extendedDay , endDay , payment , discount FROM History WHERE memberId = @memberId";
                dt = DataProvider.DataProvider.ExecuteQuery(sql, new object[] { memberId });
                if (dt.Rows.Count > 0)
                {
                    History history = new History()
                    {
                        memberId = dt.Rows[0][0].ToString(),
                        extendedDay = DateTime.Parse(dt.Rows[0][1].ToString()),
                        endDay = DateTime.Parse(dt.Rows[0][2].ToString()),
                        payment = float.Parse(dt.Rows[0][3].ToString()),
                        discount = float.Parse(dt.Rows[0][4].ToString())
                    };
                    return history;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return null;
        }

        public DataTable searchMember(string memberName)
        {
            string sql = "SELECT id , name  , birthday , phone , email, [address] FROM Member WHERE name LIKE @name and name != 'GUEST'";
            DataTable dt = new DataTable();
            try
            {
                dt = DataProvider.DataProvider.ExecuteQuery(sql, new object[] { "%" + memberName + "%" });
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            return dt;
        }

        public DataTable getAllMember()
        {
            string sql = "SELECT id, name, birthday, phone, email, address FROM Member WHERE name != 'GUEST'";
            DataTable dt = new DataTable();
            try
            {
                dt = DataProvider.DataProvider.ExecuteQuery(sql);
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            return dt;
        }

        public MemberDAO memberDAO;

        public MemberDAO _memberDAO
        {
            get { return memberDAO; }
            set { memberDAO = value; }
        }

        public MemberDAO() { }

        public bool AddNewMember(Member MemberDTO)
        {
            string query = "INSERT INTO Member VALUES( @id , @name , @sex , @birthday , @phone , @email , @address , @status )";
            int result = DataProvider.DataProvider.ExecuteNonQuery(query, new object[] { MemberDTO.id, MemberDTO.name, MemberDTO.sex, MemberDTO.birthday, MemberDTO.phone, MemberDTO.email, MemberDTO.address, MemberDTO.status });
            return result > 0;
        }

        public DataTable CheckPK(string id)
        {
            DataTable dt = new DataTable();
            string query = "Select id From Member Where id = @id";
            dt = DataProvider.DataProvider.ExecuteQuery(query, new object[] { id });
            return dt;
        }

        public DataTable SearchMember(string name)
        {
            DataTable dt = new DataTable();
            string query = "Select * From Member Where name LIKE @name AND id != 'GUEST'";
            dt = DataProvider.DataProvider.ExecuteQuery(query, new object[] { '%' + name + '%' });
            return dt;
        }

        public bool UpdateMemberInformation(Member MemberDTO)
        {
            string query = " Update Member Set name = @name , sex = @sex , birthday = @birthday , phone = @phone , email = @email , address = @address Where id = @id ";
            int result = DataProvider.DataProvider.ExecuteNonQuery(query, new object[] { MemberDTO.name, MemberDTO.sex, MemberDTO.birthday, MemberDTO.phone, MemberDTO.email, MemberDTO.address, MemberDTO.id });
            return result > 0;
        }

        public bool DisableMember(string id)
        {
            string query = "Update Member Set status = @status Where id = @id ";
            int result = DataProvider.DataProvider.ExecuteNonQuery(query, new object[] { Member.STATUS_DISABLED, id });
            return result > 0;
        }

        public bool ActiveMember(string id)
        {
            string query = "Update Member Set status = @status Where id = @id ";
            int result = DataProvider.DataProvider.ExecuteNonQuery(query, new object[] { Member.STATUS_ACTIVE, id });
            return result > 0;
        }
    }
}
