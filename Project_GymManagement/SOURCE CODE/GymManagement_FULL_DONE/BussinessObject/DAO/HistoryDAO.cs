﻿using BussinessObject.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessObject.DAO
{
    public class HistoryDAO
    {

        public History GetHistory(string memberId)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "SELECT memberId , extendedDay , endDay , payment , discount FROM History WHERE memberId = @memberId";
                dt = DataProvider.DataProvider.ExecuteQuery(sql, new object[] { memberId });
                if (dt.Rows.Count > 0)
                {
                    History history = new History()
                    {
                        memberId = dt.Rows[0][0].ToString(),
                        extendedDay = DateTime.Parse(dt.Rows[0][1].ToString()),
                        endDay = DateTime.Parse(dt.Rows[0][2].ToString()),
                        payment = float.Parse(dt.Rows[0][3].ToString()),
                        discount = float.Parse(dt.Rows[0][4].ToString())
                    };
                    return history;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return null;
        }

        public bool extendDay(History history)
        {
            try
            {
                string sql = "INSERT INTO History VALUES ( @id , @extendDay , @endDay , @payment , @discount ) ";
                return DataProvider.DataProvider.ExecuteNonQuery(sql, new object[] { history.memberId, history.extendedDay, history.endDay, history.payment, history.discount }) > 0;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
