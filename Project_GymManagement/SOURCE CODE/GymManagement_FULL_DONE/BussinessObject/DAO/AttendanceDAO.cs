﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserControlProject.DTO;

namespace BussinessObject
{
    public class AttendanceDAO
    {

        public DataTable getAllAtendance()
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "SELECT memberId FROM Attendance";
                dt = DataProvider.DataProvider.ExecuteQuery(sql);
                return dt;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public DataTable getAttendanceOneMember(string memberID)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "SELECT COUNT(a.memberId) SoBuoiTap " +
                            "FROM Attendance a " +
                            "WHERE a.memberId = @memberId " +
                            "GROUP BY a.memberId";
                dt = DataProvider.DataProvider.ExecuteQuery(sql, new object[] { memberID });
                return dt;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public DataTable getTopFiveMember(DateTime dateFrom, DateTime dateTo)
        {
            DataTable dt = new DataTable();
            try
            {
                String sql = "SELECT TOP 5 a.memberId, COUNT(a.memberId) SoBuoiTap " +
                            "FROM Attendance a " +
                            "WHERE a.date >= @dateFrom and a.date <= @dateTo and a.memberId != 'GUEST' " +
                            "GROUP BY a.memberId " +
                            "ORDER BY SoBuoiTap DESC";
                dt = DataProvider.DataProvider.ExecuteQuery(sql, new object[] { dateFrom, dateTo });
                return dt;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public DataTable GetNumberOfTraining(DateTime dateFrom, DateTime dateTo)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "SELECT a.memberId, COUNT(a.memberId) SoBuoiTap " +
                            "FROM Attendance a " +
                            "WHERE a.date >= @dateFrom and a.date <= @dateTo and a.memberId != 'GUEST' " +
                            "GROUP BY a.memberId";
                dt = DataProvider.DataProvider.ExecuteQuery(sql, new object[] { dateFrom, dateTo });
                return dt;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message); 
            }
        }

        public DataTable searchDateFromTo(DateTime dateFrom, DateTime dateTo)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "SELECT m.id, m.name, m.birthday, m.phone, m.address " +
                    "FROM Member m, (SELECT h.memberId " +
                                    "FROM Attendance h " +
                                    "WHERE h.date >= @dateFrom and h.date <= @dateTo " +
                                    "GROUP BY h.memberId) k " +
                    "WHERE m.id = k.memberId and m.id != 'GUEST' ";
                dt = DataProvider.DataProvider.ExecuteQuery(sql, new object[] { dateFrom, dateTo });
                return dt;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public string takeAttendanceGuest(Attendance attendance)
        {
            try
            {
                string sql = "INSERT INTO Attendance (memberId , [date] , takeBy , isGuest, payment) VALUES ( @memberId , @date , @takeBy , @isGuest , @payment )";
                bool result = DataProvider.DataProvider.ExecuteNonQuery(sql, new object[] { attendance.memberId, attendance.date, attendance.takeBy, attendance.isGuest, attendance.payment }) > 0;
                if (result)
                {
                    return "Check attendance for GUEST successful!";
                }
                else
                {
                    return "Something went wrong. Please try again later!";
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private bool isAttendance(DateTime earlyDate, DateTime lateDate, string memberID)
        {
            try
            {
                string sql = "SELECT id FROM Attendance WHERE memberId = @memberId and [date] >= @Earlydate and [date] <= @lateDate";
                DataTable dt = DataProvider.DataProvider.ExecuteQuery(sql, new object[] { memberID, earlyDate, lateDate });
                if (dt.Rows.Count == 1)
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return false;
        }

        public string takeAttendance(Attendance attendance)
        {
            try
            {
                DateTime today = DateTime.Now;
                DateTime earlyDate = new DateTime(today.Year, today.Month, today.Day, 0, 0, 0);
                DateTime lateDate = new DateTime(today.Year, today.Month, today.Day, 23, 59, 59);
                bool isAtt = isAttendance(earlyDate, lateDate, attendance.memberId);
                if (isAtt)
                {
                    return "The member is attendance already!";
                }
                else
                {
                    string sql = "INSERT INTO Attendance (memberId , [date] , takeBy , isGuest) VALUES ( @memberId , @date , @takeBy , @isGuest )";
                    bool result = DataProvider.DataProvider.ExecuteNonQuery(sql, new object[] { attendance.memberId, attendance.date, attendance.takeBy, attendance.isGuest }) > 0;
                    if (result)
                    {
                        return "Check attendance successful!";
                    }
                    else
                    {
                        return "Something went wrong. Please try again latter!";
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
