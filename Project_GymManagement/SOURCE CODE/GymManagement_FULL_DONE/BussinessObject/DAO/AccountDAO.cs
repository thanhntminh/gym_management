﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace BussinessObject
{
    public class AccountDAO
    {
        DataProvider.DataProvider dtp = null;
        public AccountDAO()
        {

        }

        public bool setStatus(string username, string status)
        {
            using (SqlConnection cnn = new SqlConnection(DataProvider.DataProvider.getConnectionString()))
            {
                string SQL = "UPDATE Account SET status=@status WHERE username=@username";
                if (cnn.State == ConnectionState.Closed) cnn.Open();
                SqlCommand cmd = new SqlCommand(SQL, cnn);
                cmd.Parameters.AddWithValue("@status", status);
                cmd.Parameters.AddWithValue("@username", username);
                int result = cmd.ExecuteNonQuery();
                return result > 0;
            }
        }
        public bool updateStaff(Account account)
        {
            using (SqlConnection cnn = new SqlConnection(DataProvider.DataProvider.getConnectionString()))
            {
                string SQL = "UPDATE Account SET name=@name,birthday=@birthday,phone=@phone," +
                    "email=@email,address=@address WHERE username=@username";
                if (cnn.State == ConnectionState.Closed) cnn.Open();
                SqlCommand cmd = new SqlCommand(SQL, cnn);
                cmd.Parameters.AddWithValue("@username", account.Username);
                cmd.Parameters.AddWithValue("@name", account.Name);
                cmd.Parameters.AddWithValue("@birthday", account.Birthday);
                cmd.Parameters.AddWithValue("@phone", account.Phone);
                cmd.Parameters.AddWithValue("@email", account.Email);
                cmd.Parameters.AddWithValue("@address", account.Address);
                int count = cmd.ExecuteNonQuery();
                return count > 0;
            }
        }

        public bool addNewStaff(Account account)
        {
            using (SqlConnection cnn = new SqlConnection(DataProvider.DataProvider.getConnectionString()))
            {
                try
                {
                    string SQL = "insert into Books(username,[password],name,birthday,phone,email,[address],[role],[status]) " +
                        "values(@username,@password,@name,@birthday,@phone,@email,@address,@role,@status)";
                    SqlCommand cmd = new SqlCommand(SQL, cnn);
                    cmd.Parameters.AddWithValue("@username", account.Username);
                    cmd.Parameters.AddWithValue("@password", account.Password);
                    cmd.Parameters.AddWithValue("@name", account.Name);
                    cmd.Parameters.AddWithValue("@birthday", account.Birthday);
                    cmd.Parameters.AddWithValue("@phone", account.Phone);
                    cmd.Parameters.AddWithValue("@email", account.Email);
                    cmd.Parameters.AddWithValue("@address", account.Address);
                    cmd.Parameters.AddWithValue("@role", Account.ROLE_STAFF);
                    cmd.Parameters.AddWithValue("@status", Account.STATUS_ACTIVE);
                    if (cnn.State == ConnectionState.Closed)
                    {
                        cnn.Open();
                    }
                    int count = cmd.ExecuteNonQuery();
                    return count > 0;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return false;
        }
        public Account CheckLogin(string username, string password)
        {
            Account account = new Account();

            try
            {
                DataTable dt = new DataTable();
                string query = "Select username, name, email, role, status From Account Where username = @username AND password = @password";
                dt = DataProvider.DataProvider.ExecuteQuery(query, new Object[] { username, password });
                if (dt.Rows.Count == 1)
                {
                    //role = dt.Rows[0][0].ToString();
                    account.Username = dt.Rows[0][0].ToString();
                    account.Name = dt.Rows[0][1].ToString();
                    account.Email = dt.Rows[0][2].ToString();
                    account.Role = dt.Rows[0][3].ToString();
                    account.Status = dt.Rows[0][4].ToString();
                    return account;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        public DataTable getAllStaffAccount()
        {
            DataTable result = null;
            using (SqlConnection cnn = new SqlConnection(DataProvider.DataProvider.getConnectionString()))
            {
                string Sql = "SELECT * FROM Account WHERE role=@role";
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                SqlCommand cmd = new SqlCommand(Sql, cnn);
                cmd.Parameters.AddWithValue("@role", Account.ROLE_STAFF);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                result = new DataTable();
                da.Fill(result);
            }
            return result;
        }
        public DataTable getAllStaffByName(string name)
        {
            DataTable result = null;
            using (SqlConnection cnn = new SqlConnection(DataProvider.DataProvider.getConnectionString()))
            {
                string Sql = "SELECT * FROM Account WHERE role=@role AND name LIKE @name";
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                SqlCommand cmd = new SqlCommand(Sql, cnn);
                cmd.Parameters.AddWithValue("@role", Account.ROLE_STAFF);
                cmd.Parameters.AddWithValue("@name", "%" + name + "%");
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                result = new DataTable();
                da.Fill(result);
            }
            return result;
        }

        public Boolean ResetPassword(string username, string password)
        {
            using (SqlConnection cnn = new SqlConnection(DataProvider.DataProvider.getConnectionString()))
            {
                String SQL = "UPDATE Account SET password=@password WHERE username=@username";
                cnn.Open();
                SqlCommand cmd = new SqlCommand(SQL, cnn);
                cmd.Parameters.AddWithValue("@username", username);
                cmd.Parameters.AddWithValue("@password", password);
                int rs = cmd.ExecuteNonQuery();
                return rs > 0;
            }
        }
        public Boolean ResetAdminPassword(string password)
        {
            return ResetPassword("admin", password);
        }

        public bool ChangePassword(string Username, string NewPassword)
        {            
            string query = "Update Account Set password = @password Where username = @username";
            int result = DataProvider.DataProvider.ExecuteNonQuery(query, new object[] { NewPassword, Username });
            return result > 0;
        }
    }
}
