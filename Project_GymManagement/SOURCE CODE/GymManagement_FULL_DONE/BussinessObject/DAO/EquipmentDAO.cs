﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserControlProject.DTO;

namespace BussinessObject
{
    public class EquipmentDAO
    {

        public Boolean AddNewEquipment(Equipment equipment, int quantity)
        {
            Boolean result = true;
            using (SqlConnection cnn = new SqlConnection(DataProvider.DataProvider.getConnectionString()))
            {
                string SQL = "INSERT INTO Equipment(brand,type,price,boughtDay,status) VALUES(@brand,@type,@price,@boughtDay,@status)";
                if (cnn.State == ConnectionState.Closed) cnn.Open();
                SqlCommand cmd = new SqlCommand(SQL, cnn);
                DateTime time = DateTime.Now;
                cmd.Parameters.AddWithValue("@price", equipment.price);
                cmd.Parameters.AddWithValue("@brand", equipment.brand);
                cmd.Parameters.AddWithValue("@type", equipment.type);
                cmd.Parameters.AddWithValue("@boughtDay", time);
                cmd.Parameters.AddWithValue("@status", Equipment.STATUS_ACTIVE);
                for (int i = 0; i < quantity; i++)
                {
                    int rs = cmd.ExecuteNonQuery();
                    if (rs <= 0) result = false;
                }
            }
            return result;
        }

        public DataTable getAllEquipment()
        {
            DataTable dt = null;
            using (SqlConnection cnn = new SqlConnection(DataProvider.DataProvider.getConnectionString()))
            {
                string SQL = "SELECT Equipment.id,Equipment.brand, EquipmentTypes.[type] AS 'typeName',Equipment.[type],Equipment.price," +
                    "Equipment.boughtDay,Equipment.[status] FROM Equipment " +
                    "LEFT JOIN EquipmentTypes ON Equipment.type = EquipmentTypes.id";
                cnn.Open();
                SqlCommand cmd = new SqlCommand(SQL, cnn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                dt = new DataTable();
                da.Fill(dt);
            }
            return dt;
        }

        Boolean SetStatus(string status, int id)
        {
            using (SqlConnection cnn = new SqlConnection(DataProvider.DataProvider.getConnectionString()))
            {
                string SQL = "UPDATE Equipment SET status=@status WHERE id=@id";
                if (cnn.State == ConnectionState.Closed) cnn.Open();
                SqlCommand cmd = new SqlCommand(SQL, cnn);
                cmd.Parameters.AddWithValue("@status", status);
                cmd.Parameters.AddWithValue("@id", id);
                int count = cmd.ExecuteNonQuery();
                return count > 0;
            }
        }
        public Boolean Disable(int id)
        {
            return SetStatus(Equipment.STATUS_DISABLED, id);
        }
        public Boolean Active(int id)
        {
            return SetStatus(Equipment.STATUS_ACTIVE, id);
        }

        public Boolean UpdateEquipemt(Equipment equipment)
        {
            using (SqlConnection cnn = new SqlConnection(DataProvider.DataProvider.getConnectionString()))
            {
                string SQL = "UPDATE Equipment SET brand=@brand,price=@price WHERE id=@id";
                cnn.Open();
                SqlCommand cmd = new SqlCommand(SQL, cnn);
                cmd.Parameters.AddWithValue("@brand", equipment.brand);
                cmd.Parameters.AddWithValue("@price", equipment.price);
                cmd.Parameters.AddWithValue("@id", equipment.id);
                int rs = cmd.ExecuteNonQuery();
                return rs > 0;
            }
        }


        public DataTable GetEquipmentList()
        {

            try
            {
                string query = "SELECT e.brand, et.type, e.price, e.boughtDay, e.status " +
                               "FROM (SELECT brand, price, boughtDay, status, type FROM Equipment ) e, EquipmentTypes et " +
                               "WHERE e.type = et.id";
                DataTable dt = new DataTable();
                dt = DataProvider.DataProvider.ExecuteQuery(query);
                return dt;
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
