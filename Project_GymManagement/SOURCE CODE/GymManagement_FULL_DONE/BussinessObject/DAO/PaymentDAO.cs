﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessObject.DAO
{
    public class PaymentDAO
    {
        public DataTable GetPaymentGuest(DateTime from, DateTime to)
        {
            DataTable dt = new DataTable();
            try
            {
                string query = "SELECT a.date, a.memberId, a.payment " +
                    "FROM Attendance a " +
                    "WHERE @from <= a.date AND @to >= a.date AND isGuest = '1'";
                dt = DataProvider.DataProvider.ExecuteQuery(query, new object[] { from, to });
                return dt;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public DataTable GetPaymentMember(DateTime from, DateTime to)
        {
            DataTable dt = new DataTable();
            try
            {
                string query = "SELECT h.memberId, h.extendedDay, h.payment, h.discount " +
                    "FROM History h " +
                    "WHERE @from <= h.extendedDay AND h.extendedDay <= @to";
                dt = DataProvider.DataProvider.ExecuteQuery(query, new object[] { from, to });
                return dt;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
