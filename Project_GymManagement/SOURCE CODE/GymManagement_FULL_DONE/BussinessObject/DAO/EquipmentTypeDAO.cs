﻿using BussinessObject.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessObject.DAO
{
    public class EquipmentTypeDAO
    {
        public bool addNewType(string type)
        {
            using (SqlConnection cnn = new SqlConnection(DataProvider.DataProvider.getConnectionString()))
            {
                string SQL = "insert into EquipmentTypes(type,status)" +
                    "values(@type,@status)";
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                SqlCommand cmd = new SqlCommand(SQL, cnn);
                cmd.Parameters.AddWithValue("@type", type);
                cmd.Parameters.AddWithValue("@status", EquipmentType.STATUS_ACTIVE);
                int count = cmd.ExecuteNonQuery();
                return count > 0;
            }
        }

        public DataTable GetAllTypes()
        {
            DataTable result = null;
            using (SqlConnection cnn = new SqlConnection(DataProvider.DataProvider.getConnectionString()))
            {
                string SQL = "SELECT EquipmentTypes.id,EquipmentTypes.[type],a.quantity,EquipmentTypes.[status] " +
                    "FROM(SELECT[type], COUNT(id) AS 'quantity' FROM Equipment GROUP BY[type]) a " +
                    "RIGHT JOIN EquipmentTypes ON a.[type] = EquipmentTypes.id";
                if (cnn.State == ConnectionState.Closed)
                    cnn.Open();
                SqlCommand cmd = new SqlCommand(SQL, cnn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                result = new DataTable();
                da.Fill(result);
            }
            return result;
        }
        public DataTable GetAllActiveTypes()
        {
            DataTable result = null;
            using (SqlConnection cnn = new SqlConnection(DataProvider.DataProvider.getConnectionString()))
            {
                string SQL = "SELECT * FROM EquipmentTypes WHERE status=@status";
                if (cnn.State == ConnectionState.Closed)
                    cnn.Open();
                SqlCommand cmd = new SqlCommand(SQL, cnn);
                cmd.Parameters.AddWithValue("@status", EquipmentType.STATUS_ACTIVE);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                result = new DataTable();
                da.Fill(result);
            }
            return result;
        }
        Boolean SetStatus(string status, int id)
        {
            using (SqlConnection cnn = new SqlConnection(DataProvider.DataProvider.getConnectionString()))
            {
                string SQL = "UPDATE EquipmentTypes SET status=@status WHERE id=@id";
                if (cnn.State == ConnectionState.Closed) cnn.Open();
                SqlCommand cmd = new SqlCommand(SQL, cnn);
                cmd.Parameters.AddWithValue("@status", status);
                cmd.Parameters.AddWithValue("@id", id);
                int count = cmd.ExecuteNonQuery();
                return count > 0;
            }
        }
        public Boolean Disable(int id)
        {
            return SetStatus(EquipmentType.STATUS_DISABLE, id);
        }
        public Boolean Active(int id)
        {
            return SetStatus(EquipmentType.STATUS_ACTIVE, id);
        }

        
    }
}
