﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessObject.DTO
{
    public class History
    {
        public string memberId { get; set; }
        public DateTime extendedDay { get; set; }
        public DateTime endDay { get; set; }
        public float payment { get; set; }
        public float discount { get; set; }
    }
}
