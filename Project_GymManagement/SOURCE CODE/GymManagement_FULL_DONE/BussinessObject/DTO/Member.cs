﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserControlProject.DTO
{
    public class Member
    {
        public static string STATUS_ACTIVE = "ACTIVE";
        public static string STATUS_DISABLED = "DISABLED";

        public string id { get; set; }
        public string name { get; set; }
        public string sex { get; set; }
        public DateTime birthday { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string status { get; set; }
    }
}
