﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessObject.DTO
{
    class EquipmentType
    {
        public static string STATUS_ACTIVE = "ACTIVE";
        public static string STATUS_DISABLE = "DISABLE";
        public int id { get; set; }
        public string type { get; set; }
        public string status { get; set; }
    }
}
