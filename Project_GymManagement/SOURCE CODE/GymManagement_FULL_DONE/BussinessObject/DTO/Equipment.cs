﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserControlProject.DTO
{
    public class Equipment
    {
        public static string STATUS_ACTIVE = "ACTIVE";
        public static string STATUS_DISABLED = "DISABLED";

        public int id { get; set; }
        public string brand { get; set; }
        public int type { get; set; }
        public float price { get; set; }
        public DateTime boughtDay { get; set; }
        public string status { get; set; }
    }
}
