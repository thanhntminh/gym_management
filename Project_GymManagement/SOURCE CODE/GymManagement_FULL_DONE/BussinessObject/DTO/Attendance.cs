﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserControlProject.DTO
{
    public class Attendance
    {

        public Int64 id { get; set; }
        public string memberId { get; set; }
        public DateTime date { get; set; }
        public string takeBy { get; set; }
        public bool isGuest { get; set; }
        public double payment { get; set; }
    }
}
