﻿using System;

namespace BussinessObject
{
    public class Account
    {
        public static string ROLE_ADMIN = "ADMIN";
        public static string ROLE_STAFF = "STAFF";
        public static string STATUS_ACTIVE = "ACTIVE";
        public static string STATUS_DISABLE = "DISABLE";
        public Account()
        {

        }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public DateTime Birthday { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Role { get; set; }
        public string Status { get; set; }

    }
}
