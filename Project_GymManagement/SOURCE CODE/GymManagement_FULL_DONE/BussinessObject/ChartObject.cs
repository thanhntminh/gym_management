﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessObject
{
    public class ChartObject
    {
        public int totalDay { get; set; }
        public int dayRemain { get; set; }
        public int dayUsed { get; set; }
    }
}
