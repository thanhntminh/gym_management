USE [master]
GO
/****** Object:  Database [GymManagement]    Script Date: 11/5/2017 12:13:09 PM ******/
CREATE DATABASE [GymManagement]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'GymManagement', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\GymManagement.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'GymManagement_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\GymManagement_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [GymManagement] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [GymManagement].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [GymManagement] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [GymManagement] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [GymManagement] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [GymManagement] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [GymManagement] SET ARITHABORT OFF 
GO
ALTER DATABASE [GymManagement] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [GymManagement] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [GymManagement] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [GymManagement] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [GymManagement] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [GymManagement] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [GymManagement] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [GymManagement] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [GymManagement] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [GymManagement] SET  DISABLE_BROKER 
GO
ALTER DATABASE [GymManagement] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [GymManagement] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [GymManagement] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [GymManagement] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [GymManagement] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [GymManagement] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [GymManagement] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [GymManagement] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [GymManagement] SET  MULTI_USER 
GO
ALTER DATABASE [GymManagement] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [GymManagement] SET DB_CHAINING OFF 
GO
ALTER DATABASE [GymManagement] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [GymManagement] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [GymManagement] SET DELAYED_DURABILITY = DISABLED 
GO
USE [GymManagement]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 11/5/2017 12:13:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[username] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[name] [nvarchar](100) NULL,
	[sex] [nvarchar](10) NULL,
	[birthday] [date] NULL,
	[phone] [varchar](15) NULL,
	[email] [varchar](50) NULL,
	[address] [nvarchar](100) NULL,
	[role] [varchar](20) NOT NULL,
	[status] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Attendance]    Script Date: 11/5/2017 12:13:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Attendance](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[memberId] [varchar](50) NOT NULL,
	[date] [datetime] NOT NULL,
	[takeBy] [varchar](50) NOT NULL,
	[isGuest] [bit] NULL,
	[payment] [float] NULL,
 CONSTRAINT [PK_Attendance_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Equipment]    Script Date: 11/5/2017 12:13:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Equipment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[brand] [nvarchar](100) NOT NULL,
	[type] [int] NOT NULL,
	[price] [float] NOT NULL,
	[boughtDay] [date] NOT NULL,
	[status] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Equipment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EquipmentTypes]    Script Date: 11/5/2017 12:13:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EquipmentTypes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](100) NOT NULL,
	[status] [varchar](20) NOT NULL,
 CONSTRAINT [PK_EquipmentTypes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[History]    Script Date: 11/5/2017 12:13:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[History](
	[memberId] [varchar](50) NOT NULL,
	[extendedDay] [datetime] NOT NULL,
	[endDay] [date] NOT NULL,
	[payment] [float] NOT NULL,
	[discount] [float] NULL,
 CONSTRAINT [PK_History] PRIMARY KEY CLUSTERED 
(
	[memberId] ASC,
	[extendedDay] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Member]    Script Date: 11/5/2017 12:13:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Member](
	[id] [varchar](50) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[sex] [nvarchar](20) NOT NULL,
	[birthday] [date] NULL,
	[phone] [varchar](15) NULL,
	[email] [varchar](50) NULL,
	[address] [nvarchar](100) NULL,
	[status] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Member] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Account] ([username], [password], [name], [sex], [birthday], [phone], [email], [address], [role], [status]) VALUES (N'admin', N'1', NULL, NULL, NULL, NULL, NULL, NULL, N'ADMIN', N'NOT_REMOVE')
INSERT [dbo].[Account] ([username], [password], [name], [sex], [birthday], [phone], [email], [address], [role], [status]) VALUES (N'namlt', N'123456', N'Le Thanh Nam', NULL, CAST(N'2017-10-29' AS Date), N'1212121212', N'namlt@gmail.com', N'166 Ha Long, Vung Tau', N'STAFF', N'ACTIVE')
INSERT [dbo].[Account] ([username], [password], [name], [sex], [birthday], [phone], [email], [address], [role], [status]) VALUES (N'namlt1', N'1', N'Trần Thành Công', NULL, CAST(N'2017-10-29' AS Date), N'', N'nam@gmail.com', N'13 Lê Thái Tổ', N'STAFF', N'ACTIVE')
INSERT [dbo].[Account] ([username], [password], [name], [sex], [birthday], [phone], [email], [address], [role], [status]) VALUES (N'namlt2', N'123456', N'Lê Thanh Nam', NULL, CAST(N'2017-10-25' AS Date), N'19002055', N'namlt2@gmail.com', N'13 Lê Thái Tổ', N'STAFF', N'ACTIVE')
INSERT [dbo].[Account] ([username], [password], [name], [sex], [birthday], [phone], [email], [address], [role], [status]) VALUES (N'namlt3', N'1', N'Lê Thanh Tùng', NULL, CAST(N'2017-10-24' AS Date), N'19002055', N'tungthanh@gmail.com', N'13 Lê Lai', N'STAFF', N'ACTIVE')
INSERT [dbo].[Account] ([username], [password], [name], [sex], [birthday], [phone], [email], [address], [role], [status]) VALUES (N'phucv', N'123456', N'Phú 17', NULL, CAST(N'1999-07-15' AS Date), N'0974137781', N'phucv@gmail.com', N'178 đường 3/2, tp Gia Lai', N'STAFF', N'ACTIVE')
SET IDENTITY_INSERT [dbo].[Attendance] ON 

INSERT [dbo].[Attendance] ([id], [memberId], [date], [takeBy], [isGuest], [payment]) VALUES (1, N'nambt3', CAST(N'2017-11-05 11:23:20.183' AS DateTime), N'namlt', 0, NULL)
INSERT [dbo].[Attendance] ([id], [memberId], [date], [takeBy], [isGuest], [payment]) VALUES (2, N'GUEST', CAST(N'2017-11-05 11:38:16.107' AS DateTime), N'namlt', 1, 75000)
SET IDENTITY_INSERT [dbo].[Attendance] OFF
SET IDENTITY_INSERT [dbo].[Equipment] ON 

INSERT [dbo].[Equipment] ([id], [brand], [type], [price], [boughtDay], [status]) VALUES (2, N'Power Sports', 1, 200000, CAST(N'2017-08-13' AS Date), N'ACTIVE')
INSERT [dbo].[Equipment] ([id], [brand], [type], [price], [boughtDay], [status]) VALUES (3, N'Việt Jack', 1, 500000, CAST(N'2017-10-20' AS Date), N'ACTIVE')
INSERT [dbo].[Equipment] ([id], [brand], [type], [price], [boughtDay], [status]) VALUES (4, N'No-opinition', 2, 300000, CAST(N'2017-10-20' AS Date), N'ACTIVE')
INSERT [dbo].[Equipment] ([id], [brand], [type], [price], [boughtDay], [status]) VALUES (5, N'Smart-Buy', 1, 2000000, CAST(N'2017-10-20' AS Date), N'ACTIVE')
INSERT [dbo].[Equipment] ([id], [brand], [type], [price], [boughtDay], [status]) VALUES (6, N'Big-Plane', 1, 1500000, CAST(N'2017-10-19' AS Date), N'DISABLED')
INSERT [dbo].[Equipment] ([id], [brand], [type], [price], [boughtDay], [status]) VALUES (7, N'Trường Hải', 1, 2000000, CAST(N'2017-10-30' AS Date), N'ACTIVE')
INSERT [dbo].[Equipment] ([id], [brand], [type], [price], [boughtDay], [status]) VALUES (8, N'Trường Hải', 1, 2000000, CAST(N'2017-10-30' AS Date), N'ACTIVE')
SET IDENTITY_INSERT [dbo].[Equipment] OFF
SET IDENTITY_INSERT [dbo].[EquipmentTypes] ON 

INSERT [dbo].[EquipmentTypes] ([id], [type], [status]) VALUES (1, N'Tạ đứng', N'ACTIVE')
INSERT [dbo].[EquipmentTypes] ([id], [type], [status]) VALUES (2, N'Tạ nằm', N'ACTIVE')
INSERT [dbo].[EquipmentTypes] ([id], [type], [status]) VALUES (3, N'Tạ Thành', N'DISABLE')
INSERT [dbo].[EquipmentTypes] ([id], [type], [status]) VALUES (4, N'Tạ Đức', N'ACTIVE')
INSERT [dbo].[EquipmentTypes] ([id], [type], [status]) VALUES (5, N'Tạ to', N'ACTIVE')
INSERT [dbo].[EquipmentTypes] ([id], [type], [status]) VALUES (6, N'njnjnjnj', N'ACTIVE')
SET IDENTITY_INSERT [dbo].[EquipmentTypes] OFF
INSERT [dbo].[History] ([memberId], [extendedDay], [endDay], [payment], [discount]) VALUES (N'nambt3', CAST(N'2017-11-04 18:01:16.523' AS DateTime), CAST(N'2017-12-04' AS Date), 100000, 5000)
INSERT [dbo].[Member] ([id], [name], [sex], [birthday], [phone], [email], [address], [status]) VALUES (N'GUEST', N'GUEST', N'UNDEFINED', NULL, NULL, NULL, NULL, N'NOT_REMOVE')
INSERT [dbo].[Member] ([id], [name], [sex], [birthday], [phone], [email], [address], [status]) VALUES (N'nambt3', N'Thanh Nam', N'MALE', CAST(N'1997-12-16' AS Date), N'01666031479', N'nam@gmail.com', N'16 Lê Lợi', N'ACTIVE')
ALTER TABLE [dbo].[Attendance]  WITH CHECK ADD  CONSTRAINT [FK_Attendance_Account] FOREIGN KEY([takeBy])
REFERENCES [dbo].[Account] ([username])
GO
ALTER TABLE [dbo].[Attendance] CHECK CONSTRAINT [FK_Attendance_Account]
GO
ALTER TABLE [dbo].[Attendance]  WITH CHECK ADD  CONSTRAINT [FK_Attendance_Member] FOREIGN KEY([memberId])
REFERENCES [dbo].[Member] ([id])
GO
ALTER TABLE [dbo].[Attendance] CHECK CONSTRAINT [FK_Attendance_Member]
GO
ALTER TABLE [dbo].[Equipment]  WITH CHECK ADD  CONSTRAINT [FK_Equipment_EquipmentTypes] FOREIGN KEY([type])
REFERENCES [dbo].[EquipmentTypes] ([id])
GO
ALTER TABLE [dbo].[Equipment] CHECK CONSTRAINT [FK_Equipment_EquipmentTypes]
GO
ALTER TABLE [dbo].[History]  WITH CHECK ADD  CONSTRAINT [FK_History_Member] FOREIGN KEY([memberId])
REFERENCES [dbo].[Member] ([id])
GO
ALTER TABLE [dbo].[History] CHECK CONSTRAINT [FK_History_Member]
GO
USE [master]
GO
ALTER DATABASE [GymManagement] SET  READ_WRITE 
GO
